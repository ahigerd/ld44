![Watt Balance](https://bitbucket.org/ahigerd/ld44/raw/master/Assets/Images/title.png)

Ludum Dare 44
-------------
**Theme:** Your Life Is Currency

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland) and [Obsequious Phosphorus](https://ldjam.com/users/obsequious-phosphorus)

Watt Balance
============
On a lonely, desolate planet, one lone rover sets out to restore power to what's left of the world's technological society. Only scavengers remain.

Starting on the surface, you can climb the mountains or descend into the caverns towards the core. There are seven ancient power stations, broken
down and in need of repair. You can also find power cells cached in the caves of the planet. Finding these places comes at a cost: The civilization
traded energy as a form of money, and opening the caches and repairing the power stations will cost you.

You have a power collector that you can use to recharge from ambient energy when you are idle. It can charge using sunlight on the surface, but as
you descend into the depths of the planet this light cannot reach you. Underground, repaired power stations can fill your batteries within a certain
range.


Controls
--------
* Move: Arrow Keys
* Jump: Space
* Boost: Space (in air)
* Confirm: Z
* Pause: Escape

![Screenshot](https://bitbucket.org/ahigerd/ld44/raw/master/screenshot.png)


License
=======
Copyright (c) 2019 Adam Higerd. Portions (c) 2017-2018 Austin Allman. Music (c) 2019 Obsequious Phosphorus.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
