using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class Utility {
  public static float Approach(float current, float target, float rate, float deadZone = 0.1f)
  {
    if (Mathf.Abs(current - target) < deadZone)
      return target;
    float time = Time.deltaTime;
    float steps = time * 60;
    return (current - target) * Mathf.Pow(0.5f, rate * steps) + target;
  }

  public static Vector2 RotateVector(Vector2 vec, float degrees) {
    float rad = degrees * Mathf.Deg2Rad;
    float cos = Mathf.Cos(rad);
    float sin = Mathf.Sin(rad);
    return new Vector2(
        vec.x * cos + vec.y * sin,
        vec.y * cos - vec.x * sin
    );
  }

  public static Vector2 RotateVectorRad(Vector2 vec, float rad) {
    float cos = Mathf.Cos(rad);
    float sin = Mathf.Sin(rad);
    return new Vector2(
        vec.x * cos + vec.y * sin,
        vec.y * cos - vec.x * sin
    );
  }

  public static int OppositeSide(int side) {
    return (side + 2) % 4;
  }

  public static Vector2 SideNormal(int side) {
    switch (side) {
      case 0: return Vector2.left;
      case 1: return Vector2.down;
      case 2: return Vector2.right;
      default: return Vector2.up;
    }
  }

  public static Vector2 SideAxis(int side) {
    return SideNormal((side + 1) % 4);
  }

  public static void Log(string format, params object[] args) {
    Debug.Log(string.Format(format, args));
  }

  public static void DrawDebugRect(Vector2 bottomLeft, Vector2 topRight)
  {
    Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
    Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);
    Debug.DrawLine(bottomLeft, bottomRight, Color.red, 0, false);
    Debug.DrawLine(bottomRight, topRight, Color.green, 0, false);
    Debug.DrawLine(topRight, topLeft, Color.blue, 0, false);
    Debug.DrawLine(topLeft, bottomLeft, Color.yellow, 0, false);
  }

  private static Dictionary<Color, Texture2D> texCache = new Dictionary<Color, Texture2D>();
  private static Texture2D GetColorTex(Color c) {
    if (texCache.ContainsKey(c)) {
      return texCache[c];
    }
    Texture2D tex = new Texture2D(1,1);
    tex.SetPixel(0, 0, c);
    tex.Apply();
    texCache[c] = tex;
    return tex;
  }

  public static void GUIPanel(Color background, Rect bounds) {
    GUIStyle s = new GUIStyle(GUI.skin.box);
    s.normal.background = GetColorTex(background);
    GUI.Box(bounds, " ", s);
  }

  public static void GUIPanel(Color border, Color background, Rect rect, float borderWidth) {
    Utility.GUIPanel(border, rect);
    rect.xMin += borderWidth;
    rect.yMin += borderWidth;
    rect.xMax -= borderWidth;
    rect.yMax -= borderWidth;
    Utility.GUIPanel(background, rect);
  }

  public static void GUILabel(float size, TextAnchor align, Rect bounds, string text) {
    GUIStyle s = new GUIStyle(GUI.skin.label);
    s.fontSize = (int)size;
    s.alignment = align;
    GUI.Label(bounds, text, s);
  }

  public static void GUILabel(float size, TextAnchor align, Rect bounds, string text, Color color) {
    GUIStyle s = new GUIStyle(GUI.skin.label);
    s.fontSize = (int)size;
    s.alignment = align;
    s.normal.textColor = color;
    GUI.Label(bounds, text, s);
  }

  public static Vector2 GUISprite(Sprite spr, float x, float y, float scale)
  {
    if (spr != null) {
      float minX = 1f, maxX = 0f, minY = 1f, maxY = 0f;
      foreach (var uv in spr.uv) {
        if (uv.x < minX) minX = uv.x;
        if (uv.x > maxX) maxX = uv.x;
        if (uv.y < minY) minY = uv.y;
        if (uv.y > maxY) maxY = uv.y;
      }
      GUI.DrawTextureWithTexCoords(new Rect(x, y, spr.rect.width * scale, spr.rect.height * scale), spr.texture, new Rect(minX, minY, maxX-minX, maxY-minY));
      return spr.rect.size * scale;
    }
    return Vector2.zero;
  }
}

