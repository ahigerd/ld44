﻿using UnityEngine.Rendering;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This enum describes the current state of the game.
/// </summary>
public enum GameState
{
  /// <summary>
  /// The game is displaying the title screen.
  /// Can proceed to Options or LevelLoad.
  /// </summary>
  Title,
  /// <summary>
  /// The game is displaying the out-of-game options panel.
  /// Can proceed to Title.
  /// </summary>
  Options,
  /// <summary>
  /// A level is loading.
  /// Can proceed to Spawning.
  /// </summary>
  LevelLoad,
  /// <summary>
  /// The player's state in the game world is being initialized.
  /// Can proceed to Playing.
  /// </summary>
  Spawning,
  /// <summary>
  /// The game is running and the player has control.
  /// Can proceed to Dead, LevelLoad, or LevelComplete.
  /// </summary>
  Playing,
  /// <summary>
  /// The player has lost control due to losing a life.
  /// Can proceed to Spawning or GameOver.
  /// </summary>
  Dead,
  /// <summary>
  /// The player has lost control due to completing a level.
  /// Can proceed to LevelLoad or GameOver.
  /// </summary>
  LevelComplete,
  /// <summary>
  /// The game is paused. This is a special state that can be entered from
  /// any state and remembers which state it needs to return to.
  /// </summary>
  Paused,
  /// <summary>
  /// The player has run out of lives and the game is over.
  /// Can proceed to Title.
  /// </summary>
  GameOver,
};

/// <summary>
/// This class provides general-purpose game lifetime controls.
/// </summary>
/// <remarks>
/// GameManager is meant to be a singleton. It should be added to a GameObject
/// in the game's only scene. There is at present no support for games that
/// transition between Unity scenes.
/// </remarks>
/// <seealso cref="LevelAwareBehaviour" />
public class GameManager : MonoBehaviour
{
  private delegate void GameEventDelegate(LevelAwareBehaviour obj);

  private static GameEventDelegate makeDelegate(string eventName)
  {
    return (GameEventDelegate)Delegate.CreateDelegate(
        typeof(GameEventDelegate),
        null,
        typeof(LevelAwareBehaviour).GetMethod(eventName)
    );
  }

  public static List<LevelAwareBehaviour> behaviours = new List<LevelAwareBehaviour>();
  public static GameManager instance = null;
  public static int score = 0;
  public static int level = 0;
  public static int lives = 0;
  public static float health = 2f;
  public static float maxHealth = 2f;
  public static float initialMaxHealth = 2f;
  public static int itemsCollected = 0;
  public static int itemsTotal = 1;
  public static int stationsComplete = 0;
  public static bool inDialog = false;
  public static GameState state = GameState.Title;
  private static GameState storedState = GameState.Title;

  [HideInInspector]
  public List<AudioSource> audioSource = new List<AudioSource>();
  [HideInInspector]
  public AudioSource musicSource = null;

  public int initialLives = 3;
  public Texture2D titleImage = null;
  public List<Sprite> healthImages = new List<Sprite>();
  public Sprite badStation, goodStation;

  private static Dictionary<string, GameEventDelegate> eventDelegates = new Dictionary<string, GameEventDelegate> {
        { "OnGameStart", makeDelegate("OnGameStart") },
        { "OnLevelStart", makeDelegate("OnLevelStart") },
        { "OnPlayerSpawn", makeDelegate("OnPlayerSpawn") },
        { "OnLifeStart", makeDelegate("OnLifeStart") },
        { "OnPlayerHeal", makeDelegate("OnPlayerHeal") },
        { "OnPlayerDamage", makeDelegate("OnPlayerDamage") },
        { "OnPlayerDeath", makeDelegate("OnPlayerDeath") },
        { "OnGameOver", makeDelegate("OnGameOver") }
    };

  private void DispatchEvent(string eventName)
  {
    GameEventDelegate d = eventDelegates[eventName];
    foreach (LevelAwareBehaviour obj in new List<LevelAwareBehaviour>(behaviours)) {
      if (obj.isActiveAndEnabled) {
        d(obj);
      }
    }
  }

  void Awake()
  {
    instance = this;
    state = GameState.Title;
    for (int i = 0; i < 3; i++) {
      GameObject audioObject = new GameObject();
      audioObject.transform.parent = gameObject.transform;
      audioSource.Add(audioObject.AddComponent<AudioSource>());
    }
    GameObject musicObject = new GameObject();
    musicObject.transform.parent = gameObject.transform;
    musicSource = musicObject.AddComponent<AudioSource>();
    musicSource.loop = true;
    musicSource.volume = 0.4f;
  }

  void Start()
  {
    PlayMusic(titleMusic);
  }

  public static bool IsRunning()
  {
    return state == GameState.LevelLoad ||
        state == GameState.Spawning ||
        state == GameState.Playing ||
        state == GameState.Dead;
  }

  public void StartGame()
  {
    PlayMusic(stageMusic);
    DispatchEvent("OnGameStart");
    level = 0;
    lives = initialLives;
    maxHealth = initialMaxHealth;
    itemsCollected = 0;
    stationsComplete = 0;
    NewLevel();
  }

  public void NewLevel()
  {
    state = GameState.LevelLoad;
    level += 1;

    DispatchEvent("OnLevelStart");
  }

  public void SpawnLife()
  {
    if (state == GameState.Dead) {
      lives -= 1;
    }
    if (lives < 0) {
      RankScore(score);
      lives = 0;
      state = GameState.GameOver;
      DispatchEvent("OnGameOver");
    } else {
      state = GameState.Spawning;
      DispatchEvent("OnPlayerSpawn");
    }
  }

  public void StartLife()
  {
    state = GameState.Playing;
    DispatchEvent("OnLifeStart");
  }

  public void SetHealth(float value, bool quiet = false)
  {
    if (value > GameManager.maxHealth) {
      value = GameManager.maxHealth;
    }
    if (value == GameManager.health) {
      // Don't send an event if this doesn't actually change anything
      return;
    }
    bool isDamage = value < GameManager.health;
    GameManager.health = value;
    if (value > 0) {
      if (!quiet)
        DispatchEvent(isDamage ? "OnPlayerDamage" : "OnPlayerHeal");
    } else {
      LoseLife();
    }
  }

  public void ModifyHealth(float amount, bool quiet = false)
  {
    SetHealth(GameManager.health + amount, quiet);
  }

  public void LoseLife()
  {
    GameManager.health = 0;
    state = GameState.Dead;
    DispatchEvent("OnPlayerDeath");
    PlayMusic(null);
  }

  private void RankScore(int newScore)
  {
    //LOADING
    int[] scores = new int[10];
    for (int i = 0; i < 10; i++) {
      scores[i] = PlayerPrefs.GetInt("score " + i, -1);
    }

    int score_ = newScore;

    if (score_ > 0) {
      if (score_ > scores[9])
        scores[9] = score_;

      for (int i = 8; i >= 0; i--) {
        if (scores[i + 1] > scores[i]) {
          int tmp = scores[i];
          scores[i] = scores[i + 1];
          scores[i + 1] = tmp;
        }
      }

      for (int i = 0; i < 10; i++) {
        PlayerPrefs.SetInt("score " + i, scores[i]);
      }
    }
  }

  private int lastWidth;
  private int lastHeight;
  void Update()
  {
    if ((state == GameState.Title || state == GameState.GameOver) && Input.anyKeyDown) {
      StartGame();
    }

    if (
      (Input.GetKeyDown("return") || Input.GetKeyDown("enter")) &&
      (Input.GetKey("left alt") || Input.GetKey("right alt") || Input.GetKey("left cmd") || Input.GetKey("right cmd"))
    ) {
      if (!Screen.fullScreen) {
        lastWidth = Screen.width;
        lastHeight = Screen.height;
        Resolution[] res = Screen.resolutions;
        Screen.SetResolution(res[res.Length - 1].width, res[res.Length - 1].height, true);
      } else {
        Screen.SetResolution(lastWidth, lastHeight, false);
      }
    }

    if (IsRunning() && Input.GetButtonDown("Cancel")) {
      storedState = state;
      state = GameState.Paused;
      Time.timeScale = 0;
    } else if (state == GameState.Paused && Input.anyKeyDown) {
      state = storedState;
      Time.timeScale = inDialog ? 0 : 1;
    }
  }

  public static void DrawLabel(GUIStyle style, String text, float x, float y, float w, float h)
  {
    Rect bounds = new Rect(x * Screen.width, y * Screen.height, w * Screen.width, h * Screen.height);
    GUI.Label(bounds, text, style);
  }

  public static void DrawBox(GUIStyle style, String text, Rect bounds)
  {
    GUI.Box(bounds, " ", style);
    GUI.Box(bounds, text, style);
  }

  public static void DrawBox(GUIStyle style, String text, float x, float y, float w, float h)
  {
    Rect bounds = new Rect(x * Screen.width, y * Screen.height, w * Screen.width, h * Screen.height);
    DrawBox(style, text, bounds);
  }

  public static void DrawTexture(Texture2D tex, float x, float y, float w, float h)
  {
    if (tex != null) {
      Rect bounds = new Rect(x * Screen.width, y * Screen.height, w * Screen.width, h * Screen.height);
      GUI.DrawTexture(bounds, tex, ScaleMode.ScaleToFit);
    }
  }

  void OnGUI()
  {
    GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
    boxStyle.fontSize = Screen.height / 20;
    boxStyle.alignment = TextAnchor.MiddleRight;

    GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
    labelStyle.fontSize = Screen.height / 16;
    labelStyle.alignment = TextAnchor.MiddleCenter;

    GUIStyle listStyle = new GUIStyle(labelStyle);
    listStyle.fontSize = boxStyle.fontSize;
    listStyle.alignment = TextAnchor.MiddleLeft;

    if (state == GameState.Paused) {
      DrawBox(boxStyle, " ", .3f, .3f, .4f, .4f);
      if ((int)(System.DateTime.Now.Millisecond) < 500) {
        DrawLabel(labelStyle, "PAUSED\n\nPress Any Key", .3f, .3f, .4f, .4f);
      }
    }

    if (state == GameState.Title || state == GameState.GameOver) {
      DrawBox(boxStyle, " ", .01f, .02f, .33f, .96f);
      DrawBox(boxStyle, " ", .37f, .02f, .59f, .1f);

      if ((int)(System.DateTime.Now.Millisecond) < 500) {
        DrawLabel(labelStyle, "Press Any Key to Start", .37f, .02f, .59f, .1f);
      }

      labelStyle.alignment = TextAnchor.UpperCenter;
      DrawLabel(labelStyle, "HIGH SCORES", .01f, .02f, .33f, .96f);

      for (int i = 0; i < 10; i++) {
        int s = PlayerPrefs.GetInt("score " + i, -1);
        if (s < 0)
          continue;
        DrawLabel(listStyle, "" + (i + 1) + ":    " + s, .02f, .02f + .085f * (i + 1), .31f, .085f);
      }

      if (titleImage != null) {
        DrawTexture(titleImage, .37f, .14f, .59f, .84f);
      }
    } else {
      /*
      DrawBox(hudBoxStyle, "Score: " + score, .004f, .002f, .2f, .05f);
      DrawBox(hudBoxStyle, "Lives: " + lives, .208f, .002f, .2f, .05f);
      DrawBox(hudBoxStyle, "Level: " + level, .412f, .002f, .2f, .05f);
      */
      /*
      GUILayout.BeginArea(new Rect(Screen.width * 0.616f, Screen.height * 0.002f, Screen.width * 0.2f, Screen.height * 0.05f));
      GetComponent<Menu>().GetMeter(Screen.width * 0.2f, Screen.height * 0.05f, health, new Color(0.5f, 0.5f, 0.5f, 0.5f), new Color(0f, 1f, 0f));
      GUILayout.EndArea();
      */

      int full = (int)health;
      int bars = (int)(health * 5) - (full * 5);
      float fraction = (health * 5) % 1f;
      if (Time.time * 2f % 1f < fraction) {
        bars++;
      }

      float screenScale = (Screen.height < 800) ? 4 : 8;
      Sprite spr;
      float y = 8;
      float x = 8;
      float batW = healthImages[0].rect.width * screenScale;
      float statH = goodStation.rect.height * 2;
      float ox = 0;
      for (int i = (int)maxHealth - 1; i >= 0; --i) {
        if (i < full) {
          spr = healthImages[5];
        } else if (i == full) {
          spr = healthImages[bars];
        } else {
          spr = healthImages[0];
        }
        float yInc = Utility.GUISprite(spr, x + ox, y, screenScale).y + 8;
        y += yInc;
        if (y > Screen.height - yInc) {
          y = 16 + statH;
          ox = 8 + batW;
        }
      }

      x += batW + 8;
      GUIStyle hudBoxStyle = new GUIStyle(GUI.skin.box);
      hudBoxStyle.fontSize = (int)(goodStation.rect.height * 2);
      hudBoxStyle.alignment = TextAnchor.MiddleRight;

      DrawBox(hudBoxStyle, String.Format("Completion: {0}%", (int)(100f * itemsCollected / itemsTotal)), new Rect(x, 8, 300, goodStation.rect.height * 3));
      x += 308;

      for (int i = 0; i < 7; i++) {
        x += Utility.GUISprite(i < stationsComplete ? goodStation : badStation, x, 8 + goodStation.rect.height / 2, 2).x + 4;
      }
    }
  }

  public static void PlaySound(AudioClip clip, int channel = 0, float volume = 1f)
  {
    if (clip == null) return;
    GameManager.instance.audioSource[channel].PlayOneShot(clip, volume);
  }

  public static void PlayMusic(AudioClip clip)
  {
    if (clip == null) {
      GameManager.instance.musicSource.Stop();
      return;
    }
    GameManager.instance.musicSource.clip = clip;
    GameManager.instance.musicSource.Play();
  }

  public AudioClip titleMusic, stageMusic, endingMusic;
}
