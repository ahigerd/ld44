﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
  Idle,
  Walking,
  InAir,
  Interacting
}

public class PlayerController : CharacterCore
{
  private const float TERMINAL_VELOCITY = 20f;

  public AudioClip jumpSound, thrustSound, fireSound, hurtSound;
  private AudioSource thrustAudioSource;
  private bool dying = false;
  private float deathLerp = 0;
  private bool playedDeathAnimation = false;
  //private float deathHue = 0f, deathSaturation = 0f, deathValue = 0f;
  private float debounceTimer = -1f;
  private GameObject bulletPrefab;

  [HideInInspector]
  public Vector2 velocity = new Vector2(0, 0);

  [HideInInspector]
  public float horizontal_momentum = 0f;

  private bool hasIdleAnim = false;
  private float deathTime = -1f;
  private float jumpBuffer = 0f;
  private float jumpStart = 0f;
  private bool jumping = false;
  private bool canBoost = false;
  private bool boosting = false;
  private PlayerState playerState = PlayerState.InAir;
  private float blockedTime = 0f;
  private float blinkTime = 0f;
  private float rechargeTime = 0f;

  public bool Blinking {
    get { return blinkTime > 0; }
  }

  public int HorizontalDirection
  {
    get
    {
      float raw = Input.GetAxisRaw("Horizontal");
      if (Mathf.Abs(raw) < 0.2) return 0;
      return raw < 0 ? -1 : 1;
    }
  }

  public Vector2 FindStartPoint()
  {
    return MapManager.instance.GetStartPosition();
  }

  public override void OnGameStart()
  {
    GameManager.lives = 0;
    //tileMap.NewLevel();
    playedDeathAnimation = false;
  }

  public override void OnLevelStart()
  {
    //GameManager.instance.SpawnLife();
  }

  public override void OnPlayerSpawn()
  {
    debounceTimer = Time.time + 0.25f;
    GameManager.health = GameManager.maxHealth;
    blockedTime = 0;
    deathTime = 0;
    dying = false;
    deathLerp = 0;
    origin = FindStartPoint();
    velocity = Vector2.zero;
    GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", 0);
    GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", 0);
    GameManager.instance.StartLife();
  }

  public override void OnPlayerDeath()
  {
    boosting = false;
    dying = true;
    playedDeathAnimation = false;
    if (deathSound != null)
      audioSource.PlayOneShot(deathSound);
  }

  public override void OnGameOver()
  {
    // so that the first spawn doesn't leave a corpse
    playedDeathAnimation = false;
  }

  public override void Start()
  {
    base.Start();
    origin = FindStartPoint();
    hasIdleAnim = GetComponent<SpriteAnimationManager>().FindAnimation("Idle") > -1;

    GameObject thrustHolder = new GameObject();
    thrustAudioSource = thrustHolder.AddComponent<AudioSource>();
    thrustAudioSource.loop = true;
    thrustAudioSource.clip = thrustSound;

    //bulletPrefab = (GameObject)Resources.Load("Bullet");
  }

  private float panX = 0, panY = 0;

  public void Update()
  {
    if (GameManager.state != GameState.Playing && GameManager.state != GameState.Spawning && GameManager.state != GameState.Dead) {
      return;
    }

    CameraFollower cf = GetComponent<CameraFollower>();
    if (cf.panZone == 0) {
      panX = 0;
      panY = 0;
    } else {
      if (panX == 0 && panY == 0) {
        if (cf.panZone == 1) {
          panY = +1;
        } else if (cf.panZone == 2) {
          panY = -1;
        } else if (cf.panZone == 3) {
          panX = -1;
        } else {
          panX = +1;
        }
      }
      Move(new Vector2(panX * 2f, panY * 2f));
      return;
    }

    if (blinkTime > 0) {
      blinkTime -= Time.deltaTime;
      if (blinkTime <= 0 || Mathf.Floor(blinkTime * 20f) % 2 == 0) {
        GetComponent<SpriteRenderer>().enabled = true;
        //GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0f);
      } else {
        GetComponent<SpriteRenderer>().enabled = false;
        //GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0.4f);
      }
    } else {
      GetComponent<SpriteRenderer>().enabled = true;
    }

    if (!boosting && thrustAudioSource.volume > 0) {
      thrustAudioSource.volume -= Time.deltaTime * 3f;
      if (thrustAudioSource.volume < 0) {
        thrustAudioSource.Stop();
      }
    }

    if (dying) {
      deathLerp += Time.deltaTime;
      /*
      GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", Mathf.Lerp(0f, deathHue, deathLerp));
      GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", Mathf.Lerp(0f, deathSaturation, deathLerp));
      GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", Mathf.Lerp(0f, deathValue, deathLerp));
      */
      if (!IsGrounded()) {
        GetComponent<SpriteAnimationManager>().SwitchAnimation("JumpDown", false);
        if (Blinking) blinkTime = 1; // keep blinking until you hit the ground
        velocity.y += GRAVITY * Time.deltaTime;
        if (velocity.y > TERMINAL_VELOCITY) velocity.y = TERMINAL_VELOCITY;
        velocity = Move(velocity);
      } else {
        if (!playedDeathAnimation) {
          GameManager.PlayMusic(GameManager.instance.endingMusic);
          GetComponent<SpriteRenderer>().enabled = true;
          if (Blinking) {
            gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Destroyed", true);
          } else {
            gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Discharged", true);
          }
          playedDeathAnimation = true;
        }
        deathTime += Time.deltaTime;
        if (deathTime > 5f) {
          GameManager.instance.SpawnLife();
        }
      }
      return;
    }

    if (Time.time > debounceTimer) {
      DoMovement();
      DoAction();
    }

    if (tileMap.IsSolid(midpoint.x, midpoint.y)) {
      blockedTime += Time.deltaTime;
      GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", -blockedTime);
    } else if (blockedTime > 0) {
      GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", 0);
      blockedTime = 0f;
    }
    if (blockedTime > 1.0f) {
      GameManager.instance.LoseLife();
    }

    // TODO: must be in light
    bool canCharge = origin.y < MapManager.ZONE_HEIGHT * MapManager.ROOM_HEIGHT;
    if (!canCharge) {
      foreach (Pickup station in MapManager.instance.stations) {
        if (!station.collected) continue;
        if ((new Vector2(station.transform.position.x - transform.position.x, station.transform.position.y - transform.position.y)).magnitude < MapManager.ROOM_WIDTH * 2) {
          canCharge = true;
        }
      }
    }
    if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) < 0.2f && canCharge && IsGrounded() && !Blinking && velocity.magnitude < 0.01f) {
      rechargeTime += Time.deltaTime;
      // TODO: deploy solar panel or charge antenna animation
      if (rechargeTime > 2f) {
        gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Charging", true);
        GameManager.instance.ModifyHealth(Time.deltaTime * .05f, true);
        // TODO: recharge sound every x ms
      } else if (rechargeTime > 1f) {
        gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("ChargeReady", true);
      }
    } else {
      rechargeTime = 0;
    }
    if (velocity.magnitude > 0.01f) {
      GameManager.instance.ModifyHealth(Time.deltaTime * -.01f, true);
      if (boosting) {
        GameManager.instance.ModifyHealth(Time.deltaTime * -.04f, true);
      }
    }
  }

  private bool ShouldStartJump()
  {
    if (!IsGrounded()) {
      return false;
    }
    return (Time.time - jumpBuffer) < 0.05;
  }

  private void StartJump()
  {
    if (jumpSound != null)
      audioSource.PlayOneShot(jumpSound);

    jumpStart = Time.time;
    velocity.y = -jumpPower;

    isGrounded = false;
    jumpBuffer = 0f;
    jumping = true;
    playerState = PlayerState.InAir;
  }

  public void DoMovement()
  {
    bool wasGrounded = isGrounded;
    int xAxis = HorizontalDirection;

    if (Input.GetButtonDown("Jump")) {
      jumpBuffer = Time.time;
    }
    jumping = jumping && Input.GetButton("Jump");


    if (IsGrounded() && playerState != PlayerState.Interacting)
      playerState = PlayerState.Idle;

    switch (playerState) {
      case PlayerState.Walking:
      case PlayerState.Idle:
        DoGrounded();
        break;
      case PlayerState.InAir:
        DoAir();
        break;
    }

    velocity = Move(velocity);

    if (dying) return;

    SpriteAnimationManager sam = gameObject.GetComponent<SpriteAnimationManager>();
    if (!wasGrounded && isGrounded) {
      sam.PlayOnce("Landing");
    } else if (wasGrounded && !isGrounded) {
      sam.PlayOnce("JumpStart");
    }
    if (boosting) {
      sam.SwitchAnimation("Boost", true);
    } else if (playerState == PlayerState.Walking) {
      sam.SwitchAnimation("Walking", true);
    } else if (playerState == PlayerState.Idle) {
      if (rechargeTime == 0) {
        if (hasIdleAnim)
          sam.SwitchAnimation("Idle", true);
        else if (sam.CurrentAnimationName == "Walking")
          sam.Pause();
        else
          sam.SwitchAnimation("Walking", false);
      }
    } else if (Mathf.Abs(velocity.y) < 1.0) {
      sam.SwitchAnimation("JumpIdle", true);
    } else if (velocity.y < 0) {
      sam.SwitchAnimation("JumpUp", true);
    } else if (velocity.y > 0) {
      sam.SwitchAnimation("JumpDown", true);
    }

    if (playerState == PlayerState.Interacting) return;
    if (Time.time - jumpStart < 0.3) {
      if (Mathf.Abs(velocity.x) > 0.2) {
        GetComponent<SpriteRenderer>().flipX = (velocity.x < 0);
      }
    } else if (xAxis != 0) {
      GetComponent<SpriteRenderer>().flipX = (xAxis < 0);
    }
  }


  private void DoGrounded()
  {
    canBoost = false;
    boosting = false;
    horizontal_momentum = Utility.Approach(horizontal_momentum, (Input.GetAxisRaw("Horizontal") * speed), 0.4f);
    velocity.x = horizontal_momentum;

    if (ShouldStartJump()) {
      StartJump();
    } else {
      velocity.y += GRAVITY * Time.deltaTime;
      if (velocity.y > TERMINAL_VELOCITY) velocity.y = TERMINAL_VELOCITY;
    }

    playerState = (Mathf.Abs(Input.GetAxisRaw("Horizontal")) >= 0.2f && velocity.x != 0) ? PlayerState.Walking : PlayerState.Idle;

    if (!IsGrounded())
      playerState = PlayerState.InAir;
  }


  private void DoAir()
  {
    if (canBoost && Input.GetButton("Jump")) {
      if (velocity.y > 0) velocity.y = 0;
      velocity.y -= jumpPower * Time.deltaTime;
      if (velocity.y < TERMINAL_VELOCITY * -1.5f) velocity.y = TERMINAL_VELOCITY * -1.5f;
      if (!boosting) {
        thrustAudioSource.volume = 1f;
        thrustAudioSource.Play();
      }
      boosting = true;
    } else {
      if (boosting) {
        boosting = false;
      }
      if (!jumping && velocity.y < 0) {
        velocity.y += 2.5f * GRAVITY * Time.deltaTime;
      }
      velocity.y += GRAVITY * Time.deltaTime;
      if (velocity.y > TERMINAL_VELOCITY) velocity.y = TERMINAL_VELOCITY;
      if (!jumping) {
        canBoost = true;
      }

      if (ShouldStartJump()) {
        StartJump();
      }
    }

    horizontal_momentum = Utility.Approach(horizontal_momentum, Input.GetAxisRaw("Horizontal") * speed, 0.2f);
    velocity.x = horizontal_momentum;
  }

  private void DoAction()
  {
    if (GameManager.state != GameState.Playing) {
      return;
    }
    if (playerState != PlayerState.Interacting && Input.GetButtonDown("Fire")) {
      if (fireSound != null)
        audioSource.PlayOneShot(fireSound);
      bool faceLeft = GetComponent<SpriteRenderer>().flipX;
      Bullet bullet = Instantiate(bulletPrefab).GetComponent<Bullet>();
      bullet.origin = origin + new Vector2(faceLeft ? -.8f : .8f, -1f);
      bullet.moveLeft = faceLeft;
      bullet.GetComponent<SpriteRenderer>().flipX = faceLeft;
    }
  }

  public void OnCollisionEnter2D(Collision2D coll)
  {
    OnCollisionStay2D(coll);
  }

  public void OnCollisionStay2D(Collision2D coll)
  {
    if (dying || blinkTime > 0) return;

    if (coll.gameObject.tag == "Enemy") {
      if (hurtSound != null)
        audioSource.PlayOneShot(hurtSound);
      blinkTime = 1f;
      EnemyCore enemy = coll.gameObject.GetComponent<EnemyCore>();
      GameManager.instance.ModifyHealth(-enemy.contactDamage);
    }
  }

  public override Vector2 Move(Vector2 v)
  {
    Vector2 result = base.Move(v);
    return result;
  }

  void OnGUI()
  {
    if (GameManager.state == GameState.Dead && deathTime >= 2) {
      Utility.GUIPanel(new Color(0, 0, 0, (deathTime - 2) / 2), new Rect(0, 0, Screen.width, Screen.height));
    }
  }
}
