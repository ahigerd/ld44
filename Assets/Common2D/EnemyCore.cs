using UnityEngine;

public class EnemyCore : CharacterCore {
	public int minLevel = 1;
	public int maxLevel = -1;
	public int health = 1;
	public int pointValue = 100;
  public float contactDamage = .5f;

	public virtual void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Bullet") {
			Destroy(coll.gameObject);
			health--;
			if (health <= 0) {
				Kill(true);
				GameManager.score += pointValue;
			}
		}
	}
}
