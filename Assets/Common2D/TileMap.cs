using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class TileMap : LevelAwareBehaviour {
  public enum Layer {
    Background,
    Foreground,
    Overlay
  };

	private static float BACKGROUND_DEPTH = 0.1f;
	private static float TERRAIN_DEPTH = -0.3f;
	private static float OVERLAY_DEPTH = -0.4f;

	public List<EnemyCore> enemyPrefabs = new List<EnemyCore>();
	public List<EnemyCore> collectPrefabs = new List<EnemyCore>();
  private List<EnemyCore> enemies = new List<EnemyCore>();

	public Material material;
	public Texture2D backgroundTexture;
	private Material backgroundMaterial;
	private Mesh backgroundMesh;
  private Mesh mapMesh;
  private Mesh overlayMesh;

  public MapManager manager = null;
  public string mapName = "";
  private MapRoom _room = null;
  public MapRoom room {
    get {
      if (_room == null && !String.IsNullOrWhiteSpace(mapName))  {
        _room = MapRoom.LoadFromResource(mapName);
      }
      return _room;
    }
    set {
      _room = value;
    }
  }
  public byte[,] tiles {
    get {
      return room.tiles;
    }
  }
  public byte[,] background {
    get {
      return room.background;
    }
  }
  public byte[,] overlay {
    get {
      return room.overlay;
    }
  }
  public int width {
    get {
      return room.width;
    }
  }
  public int height {
    get {
      return room.height;
    }
  }
  public int roomX {
    get {
      return room.roomX;
    }
  }
  public int roomY {
    get {
      return room.roomY;
    }
  }
  public bool meshGenerated = false;

  private static float tileEpsilon = 0.0001f;
	private static float tileSize = 1f / 16f - tileEpsilon;
  private static Vector2 SubtileToUV(int set, int subtile) {
    int setX = (set / 3) * 4;
    int setY = (set % 3) * 5;
    int subtileX = subtile & 0x03;
    int subtileY = subtile >> 2;
    return new Vector2((setX + subtileX) * (tileSize + tileEpsilon) + (tileEpsilon / 2), 1f - (subtileY + setY + 1) * (tileSize + tileEpsilon) + (tileEpsilon / 2));
  }

  private static byte[,] subtiles = {
		/* .0. 0*0 .0. */ {  0,  3, 12, 15 },
		/* .0. 0*0 .1. */ {  0,  3,  4,  7 },
		/* .0. 0*1 .0. */ {  0,  1, 12, 13 },
		/* .0. 0*1 .1. */ {  0,  1,  4,  5 },
		/* .0. 1*0 .0. */ {  2,  3, 14, 15 },
		/* .0. 1*0 .1. */ {  2,  3,  6,  7 },
		/* .0. 1*1 .0. */ {  2,  1, 14, 13 },
		/* .0. 1*1 .1. */ {  2,  1,  6,  5 },
		/* .1. 0*0 .0. */ {  8, 11, 12, 15 },
		/* .1. 0*0 .1. */ {  8, 11,  4,  7 },
		/* .1. 0*1 .0. */ {  8,  9, 12, 13 },
		/* .1. 0*1 .1. */ {  8,  9,  4,  5 },
		/* .1. 1*0 .0. */ { 10, 11, 14, 15 },
		/* .1. 1*0 .1. */ { 10, 11,  6,  7 },
		/* .1. 1*1 .0. */ { 10,  9, 14, 13 },
    /* .1. 1*1 .1. */ { 10,  9,  6,  5 },
  };

	public void Prepare()
	{
    meshGenerated = false;
    mapMesh = new Mesh();
    overlayMesh = new Mesh();
		backgroundMaterial = new Material(material);
		backgroundMaterial.mainTexture = backgroundTexture;
		backgroundMesh = new Mesh();
		backgroundMesh.Clear();
    /*
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		vertices.Add(new Vector3(0, 0, 0));
		vertices.Add(new Vector3(width, 0, 0));
		vertices.Add(new Vector3(width, -height, 0));
		vertices.Add(new Vector3(0, -height, 0));
		uv.Add(new Vector2(0, 0));
		uv.Add(new Vector2(1, 0));
		uv.Add(new Vector2(1, 1));
		uv.Add(new Vector2(0, 1));
		triangles.Add(0);
		triangles.Add(1);
		triangles.Add(2);
		triangles.Add(2);
		triangles.Add(3);
		triangles.Add(0);
		backgroundMesh.vertices = vertices.ToArray();
		backgroundMesh.uv = uv.ToArray();
		backgroundMesh.triangles = triangles.ToArray();
    */

		//PopulateMesh();
	}

	public override void OnGameStart() {
		foreach (EnemyCore enemy in enemies) {
			Destroy(enemy.gameObject);
		}
		enemies.Clear();
	}

	public override void OnPlayerSpawn() {
	}

	public void NewLevel() {
    //PopulateMesh();
	}

	public byte TileTypeAt(float x, float y)
	{
		if (x < 0 || y < 0 || x >= width || y >= height) {
      return manager.TileTypeAt(roomX * MapManager.ROOM_WIDTH + x, roomY * MapManager.ROOM_HEIGHT + y);
		}

		return tiles[(int)x, (int)y];
	}

	public byte OverlayTypeAt(float x, float y)
	{
		if (x < 0 || y < 0 || x >= width || y >= height) {
      return manager.OverlayTypeAt(roomX * MapManager.ROOM_WIDTH + x, roomY * MapManager.ROOM_HEIGHT + y);
		}

		return overlay[(int)x, (int)y];
	}

	public byte BackgroundTypeAt(float x, float y)
	{
		if (x < 0 || y < 0 || x >= width || y >= height) {
      return manager.BackgroundTypeAt(roomX * MapManager.ROOM_WIDTH + x, roomY * MapManager.ROOM_HEIGHT + y);
		}

		return background[(int)x, (int)y];
	}

	public bool IsSolid(float x, float y)
	{
		byte tileType = TileTypeAt(x, y);
		return tileType >= (byte)64;
	}

  private bool IsShadow(float x, float y)
  {
    byte overlayType = OverlayTypeAt(x, y);
    return overlayType > (int)0;
  }

  private bool HasBackground(float x, float y)
  {
    byte bgType = BackgroundTypeAt(x, y);
    return bgType > (int)0;
  }

  private delegate bool CheckTileDelegate(float x, float y);

	public void PopulateMesh(Layer layer = Layer.Foreground)
	{
    CheckTileDelegate CheckTile = layer == Layer.Overlay ? (CheckTileDelegate)IsShadow :
      layer == Layer.Background ? (CheckTileDelegate)HasBackground :
      (CheckTileDelegate)IsSolid;
    Mesh mesh = layer == Layer.Foreground ? mapMesh :
      layer == Layer.Overlay ? overlayMesh :
      backgroundMesh;
    mesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
    int tilePalette = 0;
    if (layer == Layer.Overlay) {
      tilePalette = 5;
    } else {
      switch (room.zone) {
        case MapRoom.Zone.SURFACE:
          tilePalette = 0;
          break;
        case MapRoom.Zone.MOUNTAIN:
          tilePalette = 1;
          break;
        case MapRoom.Zone.CAVERNS:
          tilePalette = 2;
          break;
        case MapRoom.Zone.CORE:
          tilePalette = 3;
          break;
      };
    }
		int triangleBase = 0;
    byte[] st = { 0, 0, 0, 0 };
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
        // TODO: background/overlay tiles
        if (!CheckTile(x, y)) {
          continue;
        }
        vertices.Add(new Vector3(x, -y, 0));
        vertices.Add(new Vector3(x + .5f, -y, 0));
        vertices.Add(new Vector3(x + .5f, -y - .5f, 0));
        vertices.Add(new Vector3(x, -y - .5f, 0));

        vertices.Add(new Vector3(x + .5f, -y, 0));
        vertices.Add(new Vector3(x + 1f, -y, 0));
        vertices.Add(new Vector3(x + 1f, -y - .5f, 0));
        vertices.Add(new Vector3(x + .5f, -y - .5f, 0));

        vertices.Add(new Vector3(x, -y - .5f, 0));
        vertices.Add(new Vector3(x + .5f, -y - .5f, 0));
        vertices.Add(new Vector3(x + .5f, -y - 1f, 0));
        vertices.Add(new Vector3(x, -y - 1f, 0));

        vertices.Add(new Vector3(x + .5f, -y - .5f, 0));
        vertices.Add(new Vector3(x + 1f, -y - .5f, 0));
        vertices.Add(new Vector3(x + 1f, -y - 1f, 0));
        vertices.Add(new Vector3(x + .5f, -y - 1f, 0));

				int bits = 0;
        if (CheckTile(x, y-1)) bits |= 8;
        if (CheckTile(x-1, y)) bits |= 4;
        if (CheckTile(x+1, y)) bits |= 2;
        if (CheckTile(x, y+1)) bits |= 1;
        for (int i = 0; i < 4; i++) {
          st[i] = subtiles[bits, i];
        }
        if (st[0] == 10 && !CheckTile(x-1, y-1)) st[0] = 17;
        if (st[1] ==  9 && !CheckTile(x+1, y-1)) st[1] = 18;
        if (st[2] ==  6 && !CheckTile(x-1, y+1)) st[2] = 16;
        if (st[3] ==  5 && !CheckTile(x+1, y+1)) st[3] = 19;
        int thisPalette = tilePalette;
        if (layer != Layer.Overlay) {
          if (TileTypeAt(x, y) == (byte)66) {
            thisPalette = 4;
          } else if (UnityEngine.Random.value > .5f) {
            thisPalette += 6;
          }
        }
        for (int i = 0; i < 4; i++) {
          Vector2 uvPos = SubtileToUV(thisPalette, st[i]);
          uv.Add(new Vector2(uvPos.x,            uvPos.y + tileSize));
          uv.Add(new Vector2(uvPos.x + tileSize, uvPos.y + tileSize));
          uv.Add(new Vector2(uvPos.x + tileSize, uvPos.y));
          uv.Add(new Vector2(uvPos.x,            uvPos.y));
          triangles.Add(triangleBase);
          triangles.Add(triangleBase+1);
          triangles.Add(triangleBase+2);
          triangles.Add(triangleBase);
          triangles.Add(triangleBase+2);
          triangles.Add(triangleBase+3);
          triangleBase += 4;
        }
			}
		}
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = triangles.ToArray();
    if (layer == Layer.Foreground) {
      PopulateMesh(Layer.Background);
    } else if (layer == Layer.Background) {
      PopulateMesh(Layer.Overlay);
    } else {
      meshGenerated = true;
    }
	}

	public void Update() {
    if (room == null) {
      // This can happen if the assets get rebuilt at runtime. The game's useless anyway, so just be quiet.
      return;
    }
    Graphics.DrawMesh(backgroundMesh, transform.position + new Vector3(0, 0, TileMap.BACKGROUND_DEPTH), Quaternion.identity, backgroundMaterial, 0, null, 0, null, false, false, false);
    Graphics.DrawMesh(mapMesh, transform.position + new Vector3(0, 0, TileMap.TERRAIN_DEPTH), Quaternion.identity, material, 0, null, 0, null, false, false, false);
    Graphics.DrawMesh(overlayMesh, transform.position + new Vector3(0, 0, TileMap.OVERLAY_DEPTH), Quaternion.identity, material, 0, null, 0, null, false, false, false);
	}
}
