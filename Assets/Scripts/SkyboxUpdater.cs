using UnityEngine;

public class SkyboxUpdater : MonoBehaviour {
  private SkyboxGradient skybox;

  public Color[] zoneColors = {
    new Color(49f/255f, 77/255f, 121/255f),
    new Color(37f/255f, 108f/255f, 221f/255f),
    new Color(255f/255f, 255f/255f, 255f/255f),
    new Color(255f/255f, 0, 0),
    new Color(0, 0, 0),
    new Color(255f/255f, 255f/255f, 255f/255f)
  };

  void Start() {
    skybox = GetComponent<SkyboxGradient>();
  }

  private float lastY = 0f;
  void Update() {
    float y = -Camera.main.transform.position.y;
    if (y == lastY) {
      return;
    }
    lastY = y;
    float pos = -Camera.main.transform.position.y / (MapManager.ZONE_HEIGHT * MapManager.ROOM_HEIGHT);
    int zone = (int)pos;
    pos -= zone;
    if (zone == 0) {
      skybox.colors[0].color = zoneColors[0];
      skybox.colors[1].color = zoneColors[1];
      skybox.colors[2].color = zoneColors[2];
      skybox.colors[1].stop = Mathf.Lerp(0, .8f, pos);
    } else if (zone == 1 && pos < .1f) {
      skybox.colors[0].color = Color.Lerp(zoneColors[0], zoneColors[1], pos / .1f);
      skybox.colors[1].color = Color.Lerp(zoneColors[1], zoneColors[3], pos / .1f);
      skybox.colors[2].color = Color.Lerp(zoneColors[1], zoneColors[4], pos / .1f);
      skybox.colors[1].stop = .8f;
    } else if (zone == 1 && pos >= .1f) {
      skybox.colors[0].color = zoneColors[1];
      skybox.colors[1].color = zoneColors[3];
      skybox.colors[2].color = zoneColors[4];
      skybox.colors[1].stop = Mathf.Lerp(skybox.colors[0].stop, skybox.colors[2].stop, zone == 0 ? pos : 1f - ((pos - .1f) / .9f));
    } else if (zone == 2 && pos < .1f) {
      skybox.colors[0].color = Color.Lerp(zoneColors[3], zoneColors[3], pos / .1f);
      skybox.colors[1].color = Color.Lerp(zoneColors[4], zoneColors[4], pos / .1f);
      skybox.colors[2].color = Color.Lerp(zoneColors[4], zoneColors[5], pos / .1f);
      skybox.colors[1].stop = .8f;
    } else {
      skybox.colors[0].color = zoneColors[3];
      skybox.colors[1].color = zoneColors[4];
      skybox.colors[2].color = zoneColors[5];
      skybox.colors[1].stop = Mathf.Lerp(skybox.colors[0].stop, skybox.colors[2].stop, zone == 0 ? pos : 1f - ((pos - .1f) / .9f));
    }
    skybox.UpdateSkybox();
  }
}
