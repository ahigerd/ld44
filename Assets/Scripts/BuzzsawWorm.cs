using UnityEngine;

public class BuzzsawWorm : EnemyCore {
  public bool clockwise = true;
  private float turnState = -1;
  private int dx = 0, dy = 0;
  private float cx = 0, cy = 0;
  private bool didInit = false;

  private SpriteAnimationManager sam = null;
  private SpriteRenderer sprite { get { return GetComponentInChildren<SpriteRenderer>(); } }
  private Transform positioner { get { return sprite.gameObject.transform; } }

  private float sensorX {
    get {
      return origin.x + cx - .5f * dy * (clockwise ? 1 : -1);
    }
  }

  private float sensorY {
    get {
      if ((dx == -1 && clockwise) || (dx == 1 && !clockwise)) {
        return origin.y + cy - .5f * Mathf.Abs(dy)  - 1;
      }
      return origin.y + cy - .5f * Mathf.Abs(dy);
    }
  }

  private void initState() {
    if (didInit) return;
    didInit = true;
    turnState = -1;
    dx = clockwise ? -1 : 1;
    dy = 0;
    cx = 0;
    cy = .6f;
    clockwise = !clockwise;
    Flip();
  }

  public override void Start() {
    base.Start();
    sam = GetComponentInChildren<SpriteAnimationManager>();
    initState();

    /*
    if (MapManager.instance.IsSolid(position.transform.x, -position.transform.y - 1)) {
      orientation = 0;
    }
    */
    sam.SwitchAnimation("Crawling", true);
  }

  void Update() {
    if (turnState >= 0) {
      UpdateTurn();
      return;
    }

    Vector2 after = Move(new Vector2(dx, dy));
    if (after.x != dx || after.y != dy) {
      Flip();
    }
    bool turn = !MapManager.instance.IsSolid(sensorX, sensorY);
    if (turn) Turn();
  }

  void Flip() {
    clockwise = !clockwise;
    if (dx == 0) {
      dy = -dy;
      cy = -cy;
    } else {
      dx = -dx;
      cx = -cx;
    }
    sprite.flipX = clockwise;
  }

  void Turn() {
    sam.SwitchAnimation("Turning", true);
    sprite.flipX = !clockwise;
    turnState = 0;
  }

  void UpdateTurn() {
    turnState += Time.deltaTime;
    //origin += new Vector2(dx, dy) * Time.deltaTime * 1.6f;

    if (turnState >= sam.CurrentAnimation.duration / 1000f) {
      origin = new Vector2(origin.x + .5f * dx, origin.y + .5f * dy);
      turnState = -1;
      DoRotate(clockwise);
      sam.SwitchAnimation("Crawling", true);
      origin = new Vector2(Mathf.Round(origin.x - .5f) + .5f + .51f * dx, Mathf.Round(origin.y) + .51f * dy);
    }
  }

	public override void OnCollisionEnter2D(Collision2D coll) {
    base.OnCollisionEnter2D(coll);

    OnCollisionStay2D(coll);
  }

	public virtual void OnCollisionStay2D(Collision2D coll) {
    if (coll.gameObject.tag == "Player") {
      PlayerController p = coll.gameObject.GetComponent<PlayerController>();
      Vector2 normal = coll.GetContact(0).normal;
      p.transform.position = p.transform.position - new Vector3(normal.x, normal.y, 0) * Time.deltaTime;
    }
  }

  public void DoRotate(bool cw) {
    initState();
    if (cw) {
      int dt = dy;
      dy = dx;
      dx = -dt;
      float ct = cy;
      cy = cx;
      cx = -ct;

      positioner.localEulerAngles = new Vector3(0, 0, positioner.localEulerAngles.z - 90);
    } else {
      int dt = dy;
      dy = -dx;
      dx = dt;
      float ct = cy;
      cy = -cx;
      cx = ct;

      positioner.localEulerAngles = new Vector3(0, 0, positioner.localEulerAngles.z + 90);
    }
  }
}
