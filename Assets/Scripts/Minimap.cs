using UnityEngine;

public class Minimap : MonoBehaviour {
  public Sprite[] mapTiles = new Sprite[16];
  public Sprite currentTile = null;
  public Sprite stationBadTile = null;
  public Sprite stationGoodTile = null;

  public int scale = 4;

  void OnGUI() {
    if (GameManager.state != GameState.Playing && GameManager.state != GameState.Dead) {
      return;
    }
    int localScale = Screen.height < 900 ? scale / 2 : scale;
    int ts = localScale * 8;
    PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    int px, py;
    if (player != null) {
      px = (int)(player.origin.x / MapManager.ROOM_WIDTH);
      py = (int)(player.origin.y / MapManager.ROOM_HEIGHT);
    } else {
      Vector2 start = MapManager.instance.GetStartPosition();
      px = (int)(start.x / MapManager.ROOM_WIDTH);
      py = (int)(start.y / MapManager.ROOM_HEIGHT);
    }
    int x1 = (int)Mathf.Clamp(px - 3, 0, MapManager.MAP_WIDTH - 8);
    int y1 = (int)Mathf.Clamp(py - 3, 0, MapManager.MAP_HEIGHT - 8);
    int x2 = x1 + 7;
    int y2 = y1 + 7;
    float bx = Screen.width - ts * (9 + x1);
    float by = ts * (1 - y1);
    for (int y = y1; y <= y2; y++) {
      for (int x = x1; x <= x2; x++) {
        MapRoom room = MapManager.instance.rooms[x, y].room;
        int bits = 0;
        if (room.width > MapManager.ROOM_WIDTH || room.height > MapManager.ROOM_HEIGHT) {
          int rx = room.width / MapManager.ROOM_WIDTH;
          int ry = room.height / MapManager.ROOM_HEIGHT;
          int lx = x - room.roomX;
          int ly = y - room.roomY;
          if (ly > 0) bits |= 1;
          if (lx < rx - 1) bits |= 2;
          if (ly < ry - 1) bits |= 4;
          if (lx > 0) bits |= 8;
        }
        foreach (MapRoom.Exit ex in room.exits) {
          if (ex.dir == MapRoom.Direction.UP) bits |= 1;
          if (ex.dir == MapRoom.Direction.RIGHT) bits |= 2;
          if (ex.dir == MapRoom.Direction.DOWN) bits |= 4;
          if (ex.dir == MapRoom.Direction.LEFT) bits |= 8;
        }
        Utility.GUISprite(mapTiles[bits], bx + x * ts, by + y * ts, localScale);
      }
    }
    foreach (Pickup station in MapManager.instance.stations) {
      int x = (int)(station.transform.position.x / MapManager.ROOM_WIDTH);
      int y = (int)(-station.transform.position.y / MapManager.ROOM_HEIGHT);
      if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
        Utility.GUISprite(station.collected ? stationGoodTile : stationBadTile, bx + x * ts, by + y * ts, localScale);
      }
    }
    if (player != null && (Time.time * 1000f) % 250f < 125f) {
      Utility.GUISprite(currentTile, bx + px * ts, by + py * ts, localScale);
    }
  }
};
