using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Pickup : CharacterCore {
  public enum PickupType {
    Battery,
    Station
  };

  public PickupType pickupType;
  public int chargeCost = 0;
  public bool collected = false;
  public Sprite item = null;
  public string itemName = "";

  private SpriteAnimationManager sam;
  private bool showDialog = false;

  public override void Start() {
    base.Start();
    sam = GetComponent<SpriteAnimationManager>();
    sam.SwitchAnimation("Default", true);
  }

  public void OnCollisionEnter2D(Collision2D coll) {
    if (collected) return;
    if (coll.gameObject.tag == "Player") {
      Time.timeScale = 0f;
      showDialog = true;
      GameManager.inDialog = true;
    }
  }

  void OnGUI() {
    if (!showDialog || !GameManager.IsRunning()) return;
    float xUnit = Screen.width / 3;
    float yUnit = Screen.height / 3;
    Utility.GUIPanel(Color.black, Color.yellow, new Rect(xUnit, yUnit, xUnit, yUnit), 4);
    Utility.GUILabel(yUnit / 7, TextAnchor.MiddleCenter, new Rect(xUnit, yUnit, xUnit, yUnit / 5), "Confirm Transaction", Color.black);
    Utility.GUILabel(yUnit / 9, TextAnchor.MiddleCenter, new Rect(xUnit, yUnit * 1.2f, xUnit / 2, yUnit / 5), "Cost:", Color.black);
    if (pickupType == PickupType.Station) {
      Sprite s = GameManager.instance.healthImages[5];
      Utility.GUISprite(s, xUnit * 1.25f - s.rect.width, yUnit * 1.5f - s.rect.height, 2);
      Utility.GUILabel(yUnit / 9, TextAnchor.MiddleCenter, new Rect(xUnit * 1.5f, yUnit * 1.2f, xUnit / 2, yUnit / 5), "Service:", Color.black);
      Utility.GUILabel(yUnit / 9, TextAnchor.MiddleCenter, new Rect(xUnit * 1.5f, yUnit * 1.4f, xUnit / 2, yUnit / 5), "Repair Station", Color.black);
    } else {
      Utility.GUILabel(yUnit / 9, TextAnchor.MiddleCenter, new Rect(xUnit, yUnit * 1.4f, xUnit / 2, yUnit / 5), String.Format("{0} W", chargeCost), Color.black);
      Utility.GUILabel(yUnit / 9, TextAnchor.MiddleCenter, new Rect(xUnit * 1.5f, yUnit * 1.2f, xUnit / 2, yUnit / 5), "Purchase:", Color.black);
      if (item != null && !String.IsNullOrWhiteSpace(itemName)) {
        float offset = 4 + item.rect.width * 2;
        Utility.GUISprite(item, xUnit * 1.55f, yUnit * 1.5f - item.rect.height, 2);
        Utility.GUILabel(yUnit / 9, TextAnchor.MiddleCenter, new Rect(xUnit * 1.55f + offset, yUnit * 1.6f, xUnit / 2 - offset - 4, yUnit / 5), itemName, Color.black);
      } else if (item != null) {
        Utility.GUISprite(item, xUnit * 1.75f - item.rect.width, yUnit * 1.5f - item.rect.height, 2);
      } else {
        Utility.GUILabel(yUnit / 9, TextAnchor.MiddleCenter, new Rect(xUnit * 1.5f, yUnit * 1.6f, xUnit / 2, yUnit / 5), itemName, Color.black);
      }
    }
    bool afford = chargeCost < (int)(GameManager.health * 100);
    bool confirmed = false;
    if (afford) {
      confirmed = GUI.Button(new Rect(xUnit + 8, yUnit * 1.8f - 8, xUnit / 2 - 12, yUnit / 5), "Confirm (Fire)") || Input.GetButtonDown("Fire");
    } else {
      Utility.GUILabel(yUnit / 12, TextAnchor.MiddleCenter, new Rect(xUnit + 8, yUnit * 1.8f - 8, xUnit / 2 - 12, yUnit / 5), "Insufficient Funds", Color.gray);
    }
    bool cancelled = GUI.Button(new Rect(xUnit * 1.5f + 4, yUnit * 1.8f - 8, xUnit / 2 - 12, yUnit / 5), "Cancel (Jump)") || Input.GetButtonDown("Jump");

    if (confirmed && !cancelled) {
      sam.SwitchAnimation("Claimed", true);
      collected = true;
      GameManager.itemsCollected++;
      GameManager.health -= chargeCost * 0.01f;
      if (pickupType == PickupType.Station) {
        GameManager.stationsComplete++;
        GameManager.maxHealth--;
      } else if (pickupType == PickupType.Battery) {
        GameManager.maxHealth++;
      }
    }
    if (confirmed || cancelled) {
      showDialog = false;
      GameManager.inDialog = false;
      Time.timeScale = 1f;
    }
  }
}
