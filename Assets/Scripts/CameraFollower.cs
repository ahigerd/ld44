using UnityEngine;

public class CameraFollower : MonoBehaviour {
  public float speed = 1f;
  public int panZone = 0;

  void Update() {
    // TODO: this is temporary
    //transform.Translate(new Vector2(Input.GetAxis("Horizontal") * speed, Input.GetAxis("Vertical") * speed));
  }

  private float lastHeight;
  void LateUpdate() {
    if (!GameManager.IsRunning() || GameManager.inDialog) {
      // No action is occurring
      return;
    }
    if (Screen.height != lastHeight) {
      lastHeight = Screen.height;
      float divisor = 0f;
      float vBlocks;
      do {
        divisor += 1f;
        vBlocks = Mathf.Floor(Screen.height / 16.0f / divisor);
      } while (vBlocks > 15);
      Camera.main.orthographicSize = Screen.height / 16.0f / divisor / 2f;
      //Camera.main.orthographicSize = 200f;
    }

    float WIDTH_UNITS = MapManager.MAP_WIDTH * MapManager.ROOM_WIDTH;
    float ZONE_UNITS = MapManager.ZONE_HEIGHT * MapManager.ROOM_HEIGHT;
    int vZone = (int)(-transform.position.y / ZONE_UNITS);
    float zoneLeft = 0;
    float zoneTop = -vZone * ZONE_UNITS;
    float zoneRight = WIDTH_UNITS;
    if (vZone == 0) {
      zoneRight /= 2;
      if (transform.position.x > zoneRight) {
        zoneLeft += zoneRight;
        zoneRight += zoneRight;
      }
    }
    float zoneBottom = zoneTop - ZONE_UNITS;
    float halfHeight = Camera.main.orthographicSize;
    float halfWidth = halfHeight / Screen.height * Screen.width;
    float cameraX = Mathf.Clamp(transform.position.x, zoneLeft + halfWidth, zoneRight - halfWidth);
    float cameraY = Mathf.Clamp(transform.position.y, zoneBottom + halfHeight, zoneTop - halfHeight);

    Time.timeScale = 1f;
    panZone = 0;
    if (transform.position.y < -1f && transform.position.y > -3 * ZONE_UNITS) {
      float panDistance = Mathf.Infinity;
      if (transform.position.y < zoneBottom + .5f) {
        panDistance = .5f - (transform.position.y - zoneBottom); // 0-.5
        panZone = 1;
      } else if (transform.position.y > zoneTop - .5f) {
        panDistance = -.5f - (transform.position.y - zoneTop); // -.5-0
        panZone = 2;
      }
      if (panDistance != Mathf.Infinity) {
        cameraY -= panDistance * halfHeight * 2;
        Time.timeScale = 0.5f;
      } else if (transform.position.y > -ZONE_UNITS) {
        panDistance = transform.position.x - WIDTH_UNITS / 2;
        if (panDistance > 0 && panDistance < 0.5f) {
          Time.timeScale = 0.5f;
          cameraX -= (0.5f - panDistance) * halfWidth * 2;
          panZone = 3;
        } else if (panDistance <= 0 && panDistance > -0.5f) {
          Time.timeScale = 0.5f;
          cameraX -= (-0.5f - panDistance) * halfWidth * 2;
          panZone = 4;
        }
      }
    }

    Camera.main.transform.position = new Vector3(cameraX, cameraY, -10);
  }
};
