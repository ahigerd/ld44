using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MapRoom {
  public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
  };
  public static bool IsVertical(Direction d) {
    return d == Direction.UP || d == Direction.DOWN;
  }
  public static Direction Flip(Direction d) {
    switch (d) {
      case Direction.UP:    return Direction.DOWN;
      case Direction.RIGHT: return Direction.LEFT;
      case Direction.DOWN:  return Direction.UP;
      case Direction.LEFT:  return Direction.RIGHT;
    }
    return Direction.UP;
  }

  public enum Zone {
    SURFACE,
    MOUNTAIN,
    CAVERNS,
    CORE
  };

  public class Exit {
    public Direction dir;
    public int minPos;
    public int maxPos;
    public MapRoom dest;
  };

  public class Feature {
    public RectInt bounds;
    public char type;
  };

  public Zone zone;
  public List<Exit> exits;
  public int width;
  public int height;
  public int roomX;
  public int roomY;
  public byte[,] tiles;
  public byte[,] overlay;
  public byte[,] background;
  public bool reachable;
  public bool premade;
  public List<Feature> features = new List<Feature>();
  public static NoiseField noise = null;

  public static MapRoom LoadFromResource(string name) {
    TextAsset asset = Resources.Load<TextAsset>("Maps/" + name);
    string[] lines = asset.text.Replace("\r", "").Trim().Split(new Char[]{ '\n' });
    bool inHeader = true;
    string solidTiles = "";
    string openTiles = "";
    MapRoom room = null;
    List<MapRoom.Exit> exits = new List<MapRoom.Exit>();
    int y = 0;
    Zone zone = Zone.SURFACE;
    for (int i = 0; i < lines.Length; i++) {
      string line = lines[i];
      if (inHeader) {
        if (String.IsNullOrWhiteSpace(line)) {
          inHeader = false;
          continue;
        }
        if (line.Contains("=")) {
          string[] kv = line.Split(new Char[]{ '=' }, 2);
          string value = kv[1];
          switch (kv[0]) {
            case "solid":
              solidTiles = value;
              break;
            case "open":
              openTiles = value;
              break;
            case "zone":
              zone = (Zone)(int.Parse(value));
              break;
            case "exit":
              string[] sections = value.Split(new Char[]{ ',' }, 3);
              MapRoom.Exit ex = new MapRoom.Exit();
              ex.dir = (MapRoom.Direction)(int.Parse(sections[0]));
              ex.minPos = int.Parse(sections[1]);
              ex.maxPos = int.Parse(sections[2]);
              ex.dest = null;
              exits.Add(ex);
              break;
          };
        }
      } else {
        if (room == null) {
          int width = line.Length;
          int height = lines.Length - i;
          room = new MapRoom(width, height);
          room.exits = exits;
          room.zone = zone;
        }
        int maxX = room.width < line.Length ? room.width : line.Length;
        for (int x = 0; x < maxX; x++) {
          int tileType = solidTiles.IndexOf(line[x]);
          if (tileType < 0) {
            tileType = openTiles.IndexOf(line[x]);
            if (tileType < 0) {
              tileType = 0;
            }
          } else {
            tileType += 64;
          }
          room.tiles[x, y] = (byte)tileType;
        }
        y++;
      }
    }
    for (y = 0; y < room.height; y++) {
      for (int x = 0; x < room.width; x++) {
        byte tileType = room.tiles[x, y];
        if (tileType == (byte)0 || tileType == (byte)64) {
          // Not a feature.
          continue;
        }
        if ((y > 0 && room.tiles[x, y-1] == tileType) || (x > 0 && room.tiles[x-1, y] == tileType)) {
          // This feature has already been detected.
          continue;
        }
        Feature feature = new Feature();
        if (tileType >= (byte)64) {
          feature.type = solidTiles[(int)tileType - 64];
        } else {
          feature.type = openTiles[tileType];
        }
        feature.bounds.xMin = x;
        feature.bounds.yMin = y;
        for (int fx = x + 1; fx < room.width; fx++) {
          if (room.tiles[fx, y] != tileType) {
            feature.bounds.xMax = fx - 1;
            break;
          }
        }
        for (int fy = y + 1; fy < room.height; fy++) {
          if (room.tiles[x, fy] != tileType) {
            feature.bounds.yMax = fy - 1;
            break;
          }
        }
        room.features.Add(feature);
      }
    }
    room.premade = true;
    return room;
  }

  public MapRoom(int width, int height) {
    this.width = width;
    this.height = height;
    this.zone = Zone.SURFACE;
    this.exits = new List<Exit>();
    this.tiles = new byte[width, height];
    this.overlay = new byte[width, height];
    this.background = new byte[width, height];
    this.roomX = 0;
    this.roomY = 0;
    this.reachable = false;
    this.premade = false;
  }

  private class Split {
    public enum Result {
      OOB,
      BEFORE,
      AFTER,
      HIT
    }

    public int x1, y1, x2, y2;
    public Split(ControlPoint p1, ControlPoint p2) {
      this.x1 = (int)p1.x;
      this.y1 = (int)p1.y;
      this.x2 = (int)p2.x;
      this.y2 = (int)p2.y;
    }
    public Split(float x1, float y1, float x2, float y2) {
      this.x1 = (int)x1;
      this.y1 = (int)y1;
      this.x2 = (int)x2;
      this.y2 = (int)y2;
    }

    public float XAtY(float y) {
      float yp = Mathf.InverseLerp(y1, y2, y);
      return yp * (x2 - x1) + x1;
    }

    public Result TestH(int x, int y, float range) {
      if (y < y1 || y > y2) return Result.OOB;
      float xp = XAtY(y);
      if (Mathf.Abs(x - xp) < range) {
        return Result.HIT;
      }
      return x < xp ? Result.BEFORE : Result.AFTER;
    }

    public float YAtX(float x) {
      float xp = Mathf.InverseLerp(x1, x2, x);
      return xp * (y2 - y1) + y1;
    }

    public Result TestV(int x, int y, float range) {
      if (x < x1 || x > x2) return Result.OOB;
      float yp = YAtX(x);
      if (Mathf.Abs(y - yp) < range) {
        return Result.HIT;
      }
      return y < yp ? Result.BEFORE : Result.AFTER;
    }
  }

  private class ControlPoint {
    public float x, y;
    public float x1, y1;
    public float x2, y2;

    // transpose=true means flip the interpretation of x and y
    public ControlPoint(bool transpose, int xmin1, int xmax1, int width, int ymin1, int ymin2, int height, bool top) {
      bool isExit = xmin1 >= 0;
      if (!isExit) {
        xmin1 = (int)UnityEngine.Random.Range(width / 3f, width / 2f);
        xmax1 = (int)UnityEngine.Random.Range(xmin1, width / 6f + width / 2f);
      }
      float y, x = (xmax1 + xmin1) / 2f;
      if (isExit) {
        y = top ? 0 : height - 1;
      } else {
        bool leftExit = ymin1 >= 0 && ymin1 < height;
        bool rightExit = ymin2 >= 0 && ymin2 < height;
        if (leftExit && rightExit) {
          y = top ? Mathf.Min(ymin1, ymin2) : Mathf.Max(ymin1, ymin2);
        } else if (leftExit) {
          y = Mathf.Clamp(ymin1, 3, height - 4);
        } else if (rightExit) {
          y = Mathf.Clamp(ymin2, 3, height - 4);
        } else {
          y = top ? UnityEngine.Random.Range(2, 8) : UnityEngine.Random.Range(height - 9, height - 3);
        }
      }

      if (transpose) {
        this.x = y;
        this.y = x;
        this.x1 = y;
        this.y1 = xmin1;
        this.x2 = y;
        this.y2 = xmax1;
      } else {
        this.x = x;
        this.y = y;
        this.x1 = xmin1;
        this.y1 = y;
        this.x2 = xmax1;
        this.y2 = y;
      }
    }
  }

  public void Generate() {
    int xmin1 = -1, xmax1 = width;
    int xmin2 = -1, xmax2 = width;
    int ymin1 = -1, ymax1 = width;
    int ymin2 = -1, ymax2 = width;
    for (int i = 0; i < exits.Count; i++) {
      MapRoom.Exit ex = exits[i];
      switch (ex.dir) {
        case Direction.UP:
          xmin1 = ex.minPos;
          xmax1 = ex.maxPos;
          break;
        case Direction.RIGHT:
          ymin2 = ex.minPos;
          ymax2 = ex.maxPos;
          break;
        case Direction.DOWN:
          xmin2 = ex.minPos;
          xmax2 = ex.maxPos;
          break;
        case Direction.LEFT:
          ymin1 = ex.minPos;
          ymax1 = ex.maxPos;
          break;
      }
    }
    ControlPoint c1 = new ControlPoint(false, xmin1, xmax1, width, ymin1, ymin2, height, true);
    ControlPoint c2 = new ControlPoint(false, xmin2, xmax2, width, ymax1, ymax2, height, false);
    ControlPoint c3 = new ControlPoint(true, ymin1, ymax1, height, xmin1, xmin2, width, true);
    ControlPoint c4 = new ControlPoint(true, ymin2, ymax2, height, xmax1, xmax2, width, false);
    Split xSplit = new Split(c1, c2);
    Split ySplit = new Split(c3, c4);
    Split[] splits = {
      new Split(c3.x1, c3.y1, c1.x1, c1.y1),
      new Split(c1.x2, c1.y2, c4.x1, c4.y1),
      new Split(c3.x2, c3.y2, c2.x1, c2.y1),
      new Split(c2.x2, c2.y2, c4.x2, c4.y2),
    };
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        tiles[x, y] = (byte)64;
        Split.Result h = xSplit.TestH(x, y, 2);
        Split.Result v = ySplit.TestV(x, y, 2);
        if (h == Split.Result.HIT || v == Split.Result.HIT) {
          // Must be open to ensure navigability
          tiles[x, y] = (byte)0;
          continue;
        } else if (h == Split.Result.OOB && v == Split.Result.OOB) {
          // Must be closed to ensure boundaries
          continue;
        } else {
          int t = (h == Split.Result.BEFORE ? 0 : 1) + (v == Split.Result.BEFORE ? 0 : 2);
          Split split = splits[t];
          Split.Result r = split.TestV(x, y, 2);
          if (r == Split.Result.OOB) {
            if (h == Split.Result.BEFORE && x > split.x1) {
              tiles[x, y] = (byte)0;
            } else if (h == Split.Result.AFTER && x < split.x2) {
              tiles[x, y] = (byte)0;
            }
          } else if (r == Split.Result.HIT) {
            tiles[x, y] = (byte)0;
          } else if (r != v) {
            tiles[x, y] = (byte)0;
          }
        }
      }
    }
    bool changed = true;
    int round = 64;
    for (int y = 0; y < height; y++) {
      if (tiles[0, y] == (byte)0) tiles[0, y] = (byte)1;
      if (tiles[width - 1, y] == (byte)0) tiles[width - 1, y] = (byte)1;
    }
    for (int x = 0; x < width; x++) {
      if (tiles[x, 0] == (byte)0) tiles[x, 0] = (byte)1;
      if (tiles[x, height - 1] == (byte)0) tiles[x, height - 1] = (byte)1;
    }
    do {
      byte next = (byte)(round == 64 ? 1 : round + 1);
      changed = false;
      for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
          if (tiles[x, y] != (byte)round) continue;
          changed = true;
          if (x > 0 && tiles[x-1, y] == (byte)0) tiles[x-1, y] = next;
          if (x < width - 1 && tiles[x+1, y] == (byte)0) tiles[x+1, y] = next;
          if (y > 0 && tiles[x, y-1] == (byte)0) tiles[x, y-1] = next;
          if (y < height - 1 && tiles[x, y+1] == (byte)0) tiles[x, y+1] = next;
        }
      }
      round = next;
    } while (changed && round < 30);
    double bx = roomX * MapManager.ROOM_WIDTH;
    double by = roomY * MapManager.ROOM_HEIGHT;
    double val, n;

    for (int y = 1; y < height - 1; y++) {
      for (int x = 1; x < width - 1; x++) {
        val = tiles[x, y];
        if (val < 3) {
          n = noise.ValueAt(bx + x, by + y, 0) / (val + 1);
        } else if (val < 4) {
          n = 0;
        } else if (val < 20) {
          n = noise.ValueAt(bx + x, by + y, 0) / Mathf.Sqrt(15f - (float)val);
        } else {
          continue;
        }
        if (n < 0.5) {
          tiles[x, y] = (byte)0;
        } else {
          tiles[x, y] = (byte)64;
        }
      }
    }
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        Split.Result h = xSplit.TestH(x, y, 1.8f);
        Split.Result v = ySplit.TestV(x, y, 1.8f);
        if (h == Split.Result.HIT || v == Split.Result.HIT) {
          // Must be open to ensure navigability
          tiles[x, y] = (byte)0;
        }
      }
    }
    for (int y = 1; y < height - 1; y++) {
      for (int x = 1; x < width - 1; x++) {
        if (tiles[x, y] < (byte)64) continue;
        if (tiles[x-1, y] >= (byte)64) continue;
        if (tiles[x+1, y] >= (byte)64) continue;
        if (tiles[x, y-1] >= (byte)64 &&
            tiles[x, y+1] >= (byte)64) continue;
        tiles[x, y] = (byte)0;
      }
    }
    // Final cleanup: make sure the boundaries are exactly aligned with no one-block overhangs,
    // or else pan transitions can soft-lock
    Exit exLeft = GetExit(Direction.LEFT);
    Exit exRight = GetExit(Direction.RIGHT);
    for (int y = 0; y < height - 1; y++) {
      FixupEdgeTile(exLeft, 0, y, 1, y);
      FixupEdgeTile(exRight, width - 1, y, width - 2, y);
    }
    Exit exUp = GetExit(Direction.UP);
    Exit exDown = GetExit(Direction.DOWN);
    for (int x = 0; x < width - 1; x++) {
      FixupEdgeTile(exUp, x, 0, x, 1);
      FixupEdgeTile(exDown, x, height - 1, x, height - 2);
    }
  }

  private void FixupEdgeTile(Exit ex, int x1, int y1, int x2, int y2) {
    bool inRange = false;
    bool isSolid = tiles[x1, y1] >= (byte)64;
    if (ex != null) {
      int p = IsVertical(ex.dir) ? x1 : y1;
      inRange = p >= ex.minPos && p <= ex.maxPos;
    }
    if (inRange) {
      bool isSolid2 = tiles[x2, y2] >= (byte)64;
      if (isSolid) tiles[x1, y1] = (byte)0;
      if (isSolid2) tiles[x2, y2] = (byte)0;
    } else if (!inRange && !isSolid) {
      tiles[x1, y1] = (byte)64;
    }
  }

  public Exit GetExit(Direction d) {
    foreach (MapRoom.Exit exTest in exits) {
      if (exTest.dir == d) {
        return exTest;
      }
    }
    return null;
  }

  public Feature FindFeature(char type) {
    foreach (Feature f in features) {
      if (f.type == type) return f;
    }
    return null;
  }

  private bool[,] shadowKernel = {
    { false, false, false, false,  true, true, true, false, false, false, false },
    { false, false, false,  true,  true, true, true,  true,  true, false, false },
    { false, false,  true,  true,  true, true, true,  true,  true,  true, false },
    { false,  true,  true,  true,  true, true, true,  true,  true,  true, false },
    {  true,  true,  true,  true,  true, true, true,  true,  true,  true,  true },
    {  true,  true,  true,  true,  true, true, true,  true,  true,  true,  true },
    {  true,  true,  true,  true,  true, true, true,  true,  true,  true,  true },
    { false,  true,  true,  true,  true, true, true,  true,  true,  true, false },
    { false,  true,  true,  true,  true, true, true,  true,  true,  true, false },
    { false, false,  true,  true,  true, true, true,  true,  true, false, false },
    { false, false, false, false,  true, true, true, false, false, false, false }
  };

  public void GenerateOverlay(MapManager manager) {
    int ox = roomX * MapManager.ROOM_WIDTH;
    int oy = roomY * MapManager.ROOM_HEIGHT;
    int k = shadowKernel.GetLength(0) / 2;

    Func<int, int, byte> RelTileAt = (x, y) => {
      if (x < 0 || y < 0 || x >= width || y >= height)
        return manager.TileTypeAt(ox + x, oy + y);
      return tiles[x, y];
    };
    Func<int, int, byte> RelOverlayAt = (x, y) => {
      if (x < 0 || y < 0 || x >= width || y >= height)
        return manager.OverlayTypeAt(ox + x, oy + y);
      return overlay[x, y];
    };
    Func<int, int, byte> RelBgAt = (x, y) => {
      if (x < 0 || y < 0 || x >= width || y >= height)
        return manager.BackgroundTypeAt(ox + x, oy + y);
      return background[x, y];
    };
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        if (zone == Zone.SURFACE) {
          if (noise.ValueAt(ox + x, (oy + y) * 2f, 1) > (oy + y) / 64f + 1.5f) {
            background[x, y] = (byte)1;
          } else if (noise.ValueAt(ox + x - 1, (oy + y) * 2f, 1) > (oy + y) / 64f + 1.5f) {
            bool twoLeft = noise.ValueAt(ox + x - 2, (oy + y) * 2f, 1) > (oy + y) / 64f + 1.5f;
            if (!twoLeft) {
              // minimum two blocks wide, always
              background[x, y] = (byte)1;
            }
          }
        } else if (zone == Zone.CORE) {
          background[x, y] = (byte)1;
        } else if (zone == Zone.MOUNTAIN && ox + x < 129) {
          if (RelBgAt(x, y - 1) > (byte)0) {
            background[x, y] = (byte)1;
          }
        }
        if (tiles[x, y] < (byte)64) continue;
        if (zone == Zone.MOUNTAIN && ox + x < 129) {
          background[x, y] = (byte)1;
        }
        overlay[x, y] = (byte)1;
        for (int y1 = y - k; y1 <= y + k; y1++) {
          for (int x1 = x - k; x1 <= x + k; x1++) {
            if (shadowKernel[x1 - x + k, y1 - y + k] && RelTileAt(x1, y1) < (byte)64) {
              overlay[x, y] = (byte)0;
              break;
            }
          }
          if (overlay[x, y] != 1) {
            break;
          }
        }
      }
    }
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        if (overlay[x, y] == (byte)0) continue;
        if (RelOverlayAt(x - 1, y) == (byte)0 &&
            RelOverlayAt(x + 1, y) == (byte)0) {
          overlay[x, y] = (byte)0;
          continue;
        }
        if (RelOverlayAt(x, y - 1) == (byte)0 &&
            RelOverlayAt(x, y + 1) == (byte)0) {
          overlay[x, y] = (byte)0;
          continue;
        }
      }
      //tiles[1, y] = (byte)0;
    }
  }
};
