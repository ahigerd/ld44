using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MapManager : LevelAwareBehaviour {
  public static MapManager instance = null;

  public static int ROOM_WIDTH = 32;
  public static int ROOM_HEIGHT = 32;
  public static int ZONE_HEIGHT = 5;
  public static int MAP_WIDTH = 10;
  public static int MAP_HEIGHT = ZONE_HEIGHT * 3;
  public TileMap[,] rooms = new TileMap[MAP_WIDTH, MAP_HEIGHT];
  private Vector2Int[] verticals = {
    new Vector2Int(MAP_WIDTH / 2 + 2, 0),
    new Vector2Int(MAP_WIDTH / 2 - 1, 1),
    new Vector2Int(MAP_WIDTH / 2 - 1, 2),
    new Vector2Int((int)Mathf.Ceil(MAP_WIDTH / 3f) - 1, ZONE_HEIGHT - 1),
    new Vector2Int((int)Mathf.Floor(MAP_WIDTH * 2f / 3f), ZONE_HEIGHT - 1),
    new Vector2Int(MAP_WIDTH / 2 - 1, 2 * ZONE_HEIGHT - 1),
  };

	public Material material;
	public Texture2D backgroundTexture;
  public Pickup stationPrefab;
  public Pickup batteryPrefab;

  private int phase = 0;

  public void Awake() {
    instance = this;
    phase = 0;
  }

  public override void OnLevelStart() {
    phase = 0;
  }

  void Update() {
    switch (phase) {
      case 0:
        ResetMaps();
        break;
      case 1:
        LoadPrefabMaps();
        break;
      case 2:
        BuildExits();
        break;
      case 3:
        GenerateMaps();
        break;
      case 4:
        EnsureMeshes(false);
        break;
      case 5:
        if (GameManager.state == GameState.LevelLoad) {
          GameManager.instance.SpawnLife();
        }
        break;
      default:
        EnsureMeshes();
        return;
    }
    ++phase;
  }

  private void ResetMaps() {
    MapRoom.noise = new NoiseField((uint)DateTime.Now.Ticks, 1, 8, 12);
    maps.Clear();
    for (int y = 0; y < MAP_HEIGHT; y++) {
      for (int x = 0; x < MAP_WIDTH; x++) {
        rooms[x, y] = null;
      }
    }

    foreach (Pickup station in stations) {
      UnityEngine.Object.Destroy(station.gameObject);
    }
    stations.Clear();

    foreach (GameObject pickup in GameObject.FindGameObjectsWithTag("Pickup")) {
      UnityEngine.Object.Destroy(pickup);
    }

    foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
      UnityEngine.Object.Destroy(enemy);
    }
  }

  private void LoadPrefabMaps() {
    for (int x = MAP_WIDTH / 2; x < MAP_WIDTH; x++) {
      LoadRoom(x, 0, "space");
      LoadRoom(x, 1, "space");
    }
    LoadRoom(MAP_WIDTH / 2, 2, "testroom");
    LoadRoom(MAP_WIDTH / 2 + 2, 2, "space");
    LoadRoom(MAP_WIDTH / 2 - 1, 0, "summit");
    LoadRoom(0, 0, "power_l").zone = MapRoom.Zone.MOUNTAIN;
    LoadRoom(MAP_WIDTH - 1, ZONE_HEIGHT - 1, "power_r").zone = MapRoom.Zone.SURFACE;
    LoadRoom(0, ZONE_HEIGHT * 2 - 1, "power_l").zone = MapRoom.Zone.CAVERNS;
    LoadRoom(MAP_WIDTH / 2 - 1, (int)(ZONE_HEIGHT * 1.5f + .5f), "power_m").zone = MapRoom.Zone.CAVERNS;
    LoadRoom(MAP_WIDTH - 1, ZONE_HEIGHT * 2 - 1, "power_r").zone = MapRoom.Zone.CAVERNS;
    LoadRoom(2, ZONE_HEIGHT * 3 - 1, "power_r").zone = MapRoom.Zone.CORE;
    LoadRoom(MAP_WIDTH - 3, ZONE_HEIGHT * 3 - 1, "power_l").zone = MapRoom.Zone.CORE;
  }

  private void BuildExits() {
    for (int y = 0; y < MAP_HEIGHT; y++) {
      for (int x = 0; x < MAP_WIDTH; x++) {
        if (rooms[x, y] == null) {
          MapRoom room = BuildRoom(x, y);
        }
      }
    }
    foreach (Vector2Int v in verticals) {
      MapRoom room = rooms[v.x, v.y].room;
      MapRoom.Exit exit = new MapRoom.Exit();
      exit.dir = MapRoom.Direction.DOWN;
      exit.minPos = (int)UnityEngine.Random.Range(4, ROOM_WIDTH / 2 - 5);
      exit.maxPos = (int)UnityEngine.Random.Range(exit.minPos + 4, ROOM_WIDTH / 2);
      if (v.y % 2 == 0) {
        exit.minPos += ROOM_WIDTH / 2 - 5;
        exit.maxPos += ROOM_WIDTH / 2 - 5;
      }
      room.exits.Add(exit);
    }
    FixupExits();
    int breakCounter = 1000;
    while (!SweepReachable()) {
      --breakCounter;
      if (breakCounter < 1) break;
      int yr = (int)UnityEngine.Random.Range(0, MAP_HEIGHT - 1);
      int xr = (int)UnityEngine.Random.Range(0, MAP_WIDTH - 1);
      for (int cy = 0; cy < MAP_HEIGHT; cy++) {
        int y = (yr + cy) % MAP_HEIGHT;
        for (int cx = 0; cx < MAP_WIDTH; cx++) {
          int x = (xr + cx) % MAP_WIDTH;
          MapRoom room = rooms[x, y].room;
          if (!room.reachable || room.premade || room.exits.Count > 2) {
            continue;
          }
          int d = (int)UnityEngine.Random.Range(0, 4);
          for (int t = 0; t < 4; t++) {
            d = (d + 1) % 4;
            int ox = (d == 1) ? +1 :
                     (d == 3) ? -1 : 0;
            int oy = (d == 2) ? +1 :
                     (d == 0) ? -1 : 0;
            if (x + ox < 0 || x + ox >= MAP_WIDTH || y + oy < 0 || y + oy >= MAP_HEIGHT) {
              continue;
            }
            if ((oy == -1 && y % ZONE_HEIGHT == 0) || (oy == +1 && y % ZONE_HEIGHT == ZONE_HEIGHT - 1)) {
              // Only cross zones by explicit bridging
              continue;
            }
            if (y < ZONE_HEIGHT && (x < MAP_WIDTH / 2) != (x + ox < MAP_WIDTH / 2)) {
              // Only cross zones by explicit bridging
              continue;
            }
            if (room.GetExit((MapRoom.Direction)d) != null) {
              // Already have an exit in that direction
              continue;
            }
            MapRoom dest = rooms[x + ox, y + oy].room;
            if (dest.premade) continue;
            if (dest.reachable) {
              if (dest.exits.Count > 2) continue;
              if (UnityEngine.Random.value > 0.001f) continue;
            }
            MapRoom.Exit exit = new MapRoom.Exit();
            exit.dir = (MapRoom.Direction)d;
            if (d == 0 || d == 2) {
              MapRoom.Direction od = MapRoom.Flip(exit.dir);
              MapRoom.Exit opposite = room.GetExit(od);
              exit.minPos = (int)UnityEngine.Random.Range(4, ROOM_WIDTH / 2 - 5);
              exit.maxPos = (int)UnityEngine.Random.Range(exit.minPos + 4, ROOM_WIDTH / 2);
              if (room.roomY % 2 == 0) {
                exit.minPos += ROOM_WIDTH / 2 - 5;
                exit.maxPos += ROOM_WIDTH / 2 - 5;
              }
            } else {
              exit.minPos = (int)UnityEngine.Random.Range(4, ROOM_HEIGHT - 9);
              exit.maxPos = (int)UnityEngine.Random.Range(exit.minPos, ROOM_HEIGHT - 5);
            }
            exit.dest = null;
            room.exits.Add(exit);
            dest.reachable = true;
            break;
          }
        }
      }
      FixupExits();
    }
  }

  private void GenerateMaps() {
    foreach (TileMap map in maps) {
      if (!map.room.premade) {
        map.room.Generate();
      }
    }
    foreach (TileMap map in maps) {
      map.Prepare();
    }
    foreach (TileMap map in maps) {
      map.room.GenerateOverlay(this);
    }
    /*
    foreach (TileMap map in maps) {
      map.PopulateMesh();
    }
    */
    FillFeatureLocations();
  }

  private void EnsureMeshes(bool limitOne = true) {
    int roomX = (int)Mathf.Floor(Camera.main.transform.position.x / ROOM_WIDTH);
    int roomY = (int)Mathf.Floor(Camera.main.transform.position.y / -ROOM_HEIGHT);
    for (int y = roomY - 1; y <= roomY + 1; y++) {
      if (y < 0 || y >= MAP_HEIGHT) continue;
      for (int x = roomX - 1; x <= roomX + 1; x++) {
        if (x < 0 || x >= MAP_WIDTH) continue;
        if (rooms[x, y].meshGenerated) continue;
        rooms[x, y].PopulateMesh();
        if (limitOne) return;
      }
    }
  }

  private void FixupExits() {
    foreach (TileMap map in maps) {
      MapRoom room = map.room;
      List<MapRoom.Exit> exits = room.exits;
      foreach (MapRoom.Exit exit in exits) {
        if (exit.dest != null) {
          continue;
        }
        int x, y;
        if (MapRoom.IsVertical(exit.dir)) {
          x = room.roomX + (int)(exit.minPos / ROOM_WIDTH);
          y = room.roomY + (exit.dir == MapRoom.Direction.DOWN ? room.height / ROOM_HEIGHT : -1);
        } else {
          x = room.roomX + (exit.dir == MapRoom.Direction.RIGHT ? room.width / ROOM_WIDTH : -1);
          y = room.roomY + (int)(exit.minPos / ROOM_HEIGHT);
        }
        if (x < 0 || y < 0 || x >= MAP_WIDTH || y >= MAP_HEIGHT) {
          continue;
        }
        MapRoom other = rooms[x, y].room;
        int ox = (room.roomX - x) * MapManager.ROOM_WIDTH;
        int oy = (room.roomY - y) * MapManager.ROOM_HEIGHT;
        int offset = MapRoom.IsVertical(exit.dir) ? ox : oy;
        MapRoom.Exit flipped = new MapRoom.Exit();
        flipped.dir = MapRoom.Flip(exit.dir);
        flipped.minPos = exit.minPos + offset;
        flipped.maxPos = exit.maxPos + offset;
        flipped.dest = room;
        exit.dest = other;
        other.exits.Add(flipped);
      }
    }
  }

  private bool SweepReachable() {
    for (int y = 0; y < MAP_HEIGHT; y++) {
      for (int x = 0; x < MAP_WIDTH; x++) {
        rooms[x, y].room.reachable = false;
      }
    }
    SweepReachable(rooms[4, 0].room);
    for (int y = 0; y < MAP_HEIGHT; y++) {
      for (int x = 0; x < MAP_WIDTH; x++) {
        if (!rooms[x, y].room.reachable) {
          return false;
        }
      }
    }
    return true;
  }

  private void SweepReachable(MapRoom room) {
    if (room == null || room.reachable) {
      return;
    }
    room.reachable = true;
    foreach (MapRoom.Exit exit in room.exits) {
      SweepReachable(exit.dest);
    }
  }

  private TileMap GetRoom(int x, int y) {
    string name = string.Format("map_{0}_{1}", x, y);
    GameObject temp = transform.Find(name)?.gameObject;
    TileMap map;
    if (temp == null) {
      temp = new GameObject(name);
      temp.transform.parent = this.transform;
      temp.transform.position = new Vector3(x * ROOM_WIDTH, -y * ROOM_HEIGHT, 0);
      map = temp.AddComponent<TileMap>();
      map.manager = this;
      map.material = this.material;
      map.backgroundTexture = this.backgroundTexture;
    } else {
      map = temp.GetComponent<TileMap>();
    }
    return map;
  }

  private void PlaceRoom(TileMap map, int x, int y) {
    map.room.roomX = x;
    map.room.roomY = y;
    int w = (int)(map.width / ROOM_WIDTH);
    int h = (int)(map.height / ROOM_HEIGHT);
    for (int rx = 0; rx < w; rx++) {
      for (int ry = 0; ry < h; ry++) {
        rooms[x + rx, y + ry] = map;
      }
    }
    maps.Add(map);
  }

  private MapRoom LoadRoom(int x, int y, string name) {
    TileMap map = GetRoom(x, y);
    map.mapName = name;
    PlaceRoom(map, x, y);
    return map.room;
  }

  private MapRoom BuildRoom(int x, int y) {
    TileMap map = GetRoom(x, y);
    if (map.room == null) {
      map.room = new MapRoom(ROOM_WIDTH, ROOM_HEIGHT);
    }
    if (y < ZONE_HEIGHT) {
      if (x < MAP_WIDTH / 2) {
        map.room.zone = MapRoom.Zone.MOUNTAIN;
      } else {
        map.room.zone = MapRoom.Zone.SURFACE;
      }
    } else if (y < ZONE_HEIGHT * 2) {
      map.room.zone = MapRoom.Zone.CAVERNS;
    } else {
      map.room.zone = MapRoom.Zone.CORE;
    }
    PlaceRoom(map, x, y);
    return map.room;
  }

  public TileMap RoomAt(float x, float y)
  {
    int roomX = (int)Mathf.Floor(x / ROOM_WIDTH);
    int roomY = (int)Mathf.Floor(y / ROOM_HEIGHT);
    if (roomX < 0 || roomY < 0 || roomX >= MAP_WIDTH || roomY >= MAP_HEIGHT) {
      return null;
    }
    return rooms[roomX, roomY];
  }

  public bool IsSolid(float x, float y)
  {
    TileMap room = RoomAt(x, y);
    if (room == null) {
      return true;
    }
    return room.IsSolid(x - room.roomX * ROOM_WIDTH, y - room.roomY * ROOM_HEIGHT);
  }

  public byte TileTypeAt(float x, float y)
  {
    TileMap room = RoomAt(x, y);
    if (room == null) {
      return (byte)64;
    }
    return room.TileTypeAt(x - room.roomX * ROOM_WIDTH, y - room.roomY * ROOM_HEIGHT);
  }

  public byte OverlayTypeAt(float x, float y)
  {
    TileMap room = RoomAt(x, y);
    if (room == null) {
      return (byte)0;
    }
    return room.OverlayTypeAt(x - room.roomX * ROOM_WIDTH, y - room.roomY * ROOM_HEIGHT);
  }

  public byte BackgroundTypeAt(float x, float y)
  {
    TileMap room = RoomAt(x, y);
    if (room == null) {
      return (byte)0;
    }
    return room.BackgroundTypeAt(x - room.roomX * ROOM_WIDTH, y - room.roomY * ROOM_HEIGHT);
  }

  public Vector2 GetStartPosition() {
    foreach (TileMap map in maps) {
      MapRoom.Feature feature = map.room.FindFeature('@');
      if (feature == null) continue;
      return new Vector2(map.roomX * ROOM_WIDTH + feature.bounds.xMin + .5f, map.roomY * ROOM_HEIGHT + feature.bounds.yMin);
    }
    return Vector2.zero;
  }

  private void FillFeatureLocations() {
    GameManager.itemsTotal = 0;
    foreach (TileMap map in maps) {
      MapRoom.Feature feature = map.room.FindFeature('*');
      if (feature == null) continue;
      Vector2 pos = feature.bounds.center;
      pos += new Vector2(map.roomX * ROOM_WIDTH, map.roomY * ROOM_HEIGHT);
      Pickup pickup = Instantiate<Pickup>(
        stationPrefab,
        new Vector2(pos.x + .5f, -pos.y + 1.5f),
        Quaternion.identity
      );
      stations.Add(pickup);
      GameManager.itemsTotal++;
    }
    List<MapRoom> candidates = new List<MapRoom>();
    int added = 0;
    foreach (TileMap map in maps) {
      //if (added >= 5) break;
      if (map.room.premade) {
        continue;
      }
      if (map.room.exits.Count == 1) {
        candidates.Add(map.room);
        for (int y = map.height - 1; y >= 2; --y) {
          int bestX = -1, bestW = 1;
          for (int x = 0; x < map.width - 2; x++) {
            if (map.IsSolid(x, y) && !map.IsSolid(x, y-1) && !map.IsSolid(x, y-2)) {
              for (int x2 = x + 1; x2 < map.width; x2++) {
                if (!map.IsSolid(x2, y) || map.IsSolid(x2, y-1) || map.IsSolid(x2, y-2)) {
                  int w = x2 - x;
                  if (w > bestW) {
                    bestX = x;
                    bestW = w;
                  }
                  break;
                }
              }
            }
          }
          if (bestW >= 2) {
            float tx = map.roomX * ROOM_WIDTH + bestX + bestW / 2f;
            float ty = map.roomY * ROOM_HEIGHT + y - 1;
            Pickup pickup = Instantiate<Pickup>(
              batteryPrefab,
              new Vector2(tx, -ty),
              Quaternion.identity
            );
            GameManager.itemsTotal++;
            added++;
            break;
          }
        }
      }
    }
    //return;
    foreach (TileMap map in maps) {
      for (int ct = 0; ct < 4; ct++) {
        int ex, ey, d = 0;
        bool ok = false;
        int tries = 100;
        do {
          ex = UnityEngine.Random.Range(2, map.width - 4);
          ey = UnityEngine.Random.Range(2, map.height - 4);
          if (map.IsSolid(ex, ey)) continue;
          d = (int)UnityEngine.Random.Range(0, 3.999f);
          for (int k = 0; k < 4; k++) {
            int kx = 0, ky = 0;
            if (d == 0) ky = 1;
            if (d == 1) kx = -1;
            if (d == 2) ky = -1;
            if (d == 3) kx = 1;
            if (map.IsSolid(ex + kx, ey + ky)) {
              ok = true;
              break;
            }
          }
        } while (!ok && (--tries) > 0);
        if (!ok) continue;
        float tx = map.roomX * ROOM_WIDTH;
        float ty = map.roomY * ROOM_HEIGHT;
        BuzzsawWorm worm = Instantiate<BuzzsawWorm>(enemies[0] as BuzzsawWorm, new Vector2(tx + ex + .5f, -ty - ey - 1), Quaternion.identity);
        worm.clockwise = UnityEngine.Random.value > 0.5;
        while (d > 0) {
          --d;
          worm.DoRotate(true);
        }
      }
    }
  }

  public List<EnemyCore> enemies = new List<EnemyCore>();
  public List<Pickup> stations = new List<Pickup>();
  private List<TileMap> maps = new List<TileMap>();

  void OnGUI() {
    if (GameManager.state == GameState.LevelLoad) {
      GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
      boxStyle.fontSize = Screen.height / 20;
      boxStyle.alignment = TextAnchor.LowerRight;
      GameManager.DrawBox(boxStyle, "Loading...", 0, 0, 1, 1);
    }
  }
};
