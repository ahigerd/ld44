using UnityEngine;

class ShipBlockPrefabs {
	public Mesh cube = new Mesh();
	public Mesh hWedge = new Mesh();
	public Mesh vWedge = new Mesh();
	public Mesh point = new Mesh();
	public Mesh topSlope = new Mesh();
	public Mesh bottomSlope = new Mesh();

	public ShipBlockPrefabs() {
		cube.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.5f, -0.5f, +0.5f),
			new Vector3(+0.5f, -0.5f, +0.5f),
			new Vector3(-0.5f, +0.5f, +0.5f),
			new Vector3(+0.5f, +0.5f, +0.5f)
		};
		cube.uv = new Vector2[] {
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1)
		};
		cube.triangles = new int[] {
			0, 3, 1, 0, 2, 3, // front
			0, 1, 5, 0, 5, 4, // left
			0, 6, 2, 0, 4, 6, // top
			7, 6, 4, 7, 4, 5, // back
			7, 2, 6, 7, 3, 2, // right
			7, 5, 1, 7, 1, 3  // bottom
		};

		hWedge.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.5f, -0.1f, +0.5f),
			new Vector3(+0.5f, -0.1f, +0.5f),
			new Vector3(-0.5f, +0.1f, +0.5f),
			new Vector3(+0.5f, +0.1f, +0.5f)
		};
		hWedge.uv = cube.uv;
		hWedge.triangles = cube.triangles;

		vWedge.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.1f, -0.5f, +0.5f),
			new Vector3(+0.1f, -0.5f, +0.5f),
			new Vector3(-0.1f, +0.5f, +0.5f),
			new Vector3(+0.1f, +0.5f, +0.5f)
		};
		vWedge.uv = cube.uv;
		vWedge.triangles = cube.triangles;

		point.vertices = new Vector3[] {
			new Vector3(-0.5f, -0.5f, -0.5f),
			new Vector3(+0.5f, -0.5f, -0.5f),
			new Vector3(-0.5f, +0.5f, -0.5f),
			new Vector3(+0.5f, +0.5f, -0.5f),
			new Vector3(-0.1f, -0.1f, +0.5f),
			new Vector3(+0.1f, -0.1f, +0.5f),
			new Vector3(-0.1f, +0.1f, +0.5f),
			new Vector3(+0.1f, +0.1f, +0.5f)
		};
		point.uv = cube.uv;
		point.triangles = cube.triangles;

		topSlope.vertices = cube.vertices;
		topSlope.uv = cube.uv;
		topSlope.triangles = new int[] {
			0, 7, 1, 0, 6, 7, // front
			0, 4, 6, // left
			7, 5, 1, // right
			7, 6, 4, 7, 4, 5, // back
			0, 1, 5, 0, 5, 4, // bottom
		};

		bottomSlope.vertices = cube.vertices;
		bottomSlope.uv = cube.uv;
		bottomSlope.triangles = new int[] {
			2, 3, 5, 2, 5, 4, // front
			2, 4, 6, // left
			7, 5, 3, // right
			7, 6, 4, 7, 4, 5, // back
			2, 6, 7, 2, 7, 3, // top
		};
	}
}

[System.Serializable]
public class ShipBlock {
	private static ShipBlockPrefabs prefabs = new ShipBlockPrefabs();

	public enum Facing {
		Forward,
		Backward,
		Left,
		Right,
		Up,
		Down
	};

	public enum GenericType {
		None,
		Cube,
		HWedge,
		VWedge,
		Point,
		TopSlope,
		BottomSlope,
	};

	public Vector3 position;
	private Mesh mesh;
	public ShipBlock.Facing facing = ShipBlock.Facing.Forward;
	public GenericType genericType = GenericType.None;

	public ShipBlock(GenericType blockType, Facing facing = Facing.Forward) {
		this.facing = facing;
		this.genericType = blockType;
		switch (blockType) {
			case GenericType.Cube:
				this.mesh = prefabs.cube;
				break;
			case GenericType.HWedge:
				this.mesh = prefabs.hWedge;
				break;
			case GenericType.VWedge:
				this.mesh = prefabs.vWedge;
				break;
			case GenericType.Point:
				this.mesh = prefabs.point;
				break;
			case GenericType.TopSlope:
				this.mesh = prefabs.topSlope;
				break;
			case GenericType.BottomSlope:
				this.mesh = prefabs.bottomSlope;
				break;
		}
		/*
		if (uvOffset != null) {
			for (int i = 0; i < uv.Length; i++) {
				uv[i] += uvOffset;
			}
		}
		*/
	}

	public CombineInstance GetCombineInstance() {
		CombineInstance ci = new CombineInstance();
		if (this.genericType != GenericType.None) {
			this.mesh = new ShipBlock(this.genericType).mesh;
		}
		ci.mesh = this.mesh;
		ci.transform = Matrix4x4.Translate(this.position);
		switch (this.facing) {
			case ShipBlock.Facing.Backward:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(0, 180, 0));
				break;
			case ShipBlock.Facing.Left:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(0, -90, 0));
				break;
			case ShipBlock.Facing.Right:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(0, 90, 0));
				break;
			case ShipBlock.Facing.Up:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(90, 0, 0));
				break;
			case ShipBlock.Facing.Down:
				ci.transform = ci.transform * Matrix4x4.Rotate(Quaternion.Euler(-90, 0, 0));
				break;
		}
		return ci;
	}

	public static ShipBlock Cube = new ShipBlock(ShipBlock.GenericType.Cube);
}
