﻿//This script enables underwater effects. Attach to main camera.
/* 
//Define variables
var underwaterLevel : float = 50.8;
var deepestLevel : float = 20;
var ambientColor : Color = new Color(0.13725490196, 0.14117647058, 0.3294117647, 1);
var directionalLight : Light;
var LightingColor : Color = new Color(0.15686274509, 0.19215686274, 0.21960784313, 1);
 
//The scene's default fog settings
private var defaultFog;
private var defaultFogColor;
private var defaultFogDensity;
private var defaultSkybox;
private var defaultLighting;
private var defaultAmbient;
private var noSkybox : Material;
 
function Start () {
	defaultFog = RenderSettings.fog;
	defaultFogColor = RenderSettings.fogColor;
	defaultFogDensity = RenderSettings.fogDensity;
	defaultSkybox = RenderSettings.skybox;
	if (directionalLight != null)
		defaultLighting = directionalLight.color;
	defaultAmbient = RenderSettings.ambientLight;

    //Set the background color
    GetComponent.<Camera>().backgroundColor = Color (0, 0.4, 0.7, 1);
}
 
function Update () {
    if (transform.position.y < underwaterLevel) {
    	var i : float = Mathf.Clamp((transform.position.y - deepestLevel) / (underwaterLevel - deepestLevel), 0.0, 1.0);
    	i = WeightedCurve( i, 0.4);

    	var c1 = Color.Lerp(Color(0, 0.05, 0.2, 1), Color (0, 0.4, 0.7, 0.6), i);
    	var c2 = Color.Lerp(ambientColor, defaultAmbient, i);
    	if (directionalLight != null)
    		var c3 = Color.Lerp(LightingColor, defaultLighting, i);
        RenderSettings.fog = true;
        RenderSettings.fogColor = c1;
        RenderSettings.fogDensity = 0.04; //Mathf.Lerp(0.02, 0.04, (transform.position.y - deepestLevel) / (underwaterLevel - deepestLevel));
        RenderSettings.skybox = noSkybox;
        GetComponent.<Camera>().backgroundColor = c1;
        RenderSettings.ambientLight = c2;
        if (directionalLight != null)
        	directionalLight.color = c3;
    }
 
    else {
    	RenderSettings.ambientLight = defaultAmbient;
    	if (directionalLight != null)
        	directionalLight.color = defaultLighting;
        RenderSettings.fog = defaultFog;
        RenderSettings.skybox = defaultSkybox;
        if (GetComponent.<SkyboxGradient>() == null)
        	RenderSettings.fogColor = defaultFogColor;
        else
        	GetComponent.<SkyboxGradient>().UpdateSkybox();
        RenderSettings.fogDensity = defaultFogDensity;
    }
}

private function WeightedCurve(val : float, curve : float) : float
{
        var adj : float = Mathf.Exp(-curve);
        return Mathf.Pow(1.0 - Mathf.Pow(1.0 - val, adj), 1.0 / adj);
}*/
